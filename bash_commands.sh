SERVER_USERNAME=$SU_USERNAME2

SERVER=$SU_SERVER2

alias calcinit="ssh $SERVER_USERNAME@$SERVER \"mkdir ${PWD##*/}\""

alias calcup="rsync -avu --delete $(pwd)/ $SERVER_USERNAME@$SERVER:~/${PWD##*/}/"

function calcstart() {
        ssh $SU_USERNAME2@$SU_SERVER2 "cd ${PWD##*/}; nohup python3 -u run.py $1 > $1/out.log 2>&1 &"
}

function calcdown() {
        rsync -avu --delete $SERVER_USERNAME@$SERVER:~/${PWD##*/}/$1 $(pwd)/$1
}
