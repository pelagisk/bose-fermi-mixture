# Boson-fermion mixture

## Introduction


## Installation

Requires Python 3 and the following packages:

- `numpy`
- `scipy`
- `matplotlib`
- `pandas`

They can be installed from a terminal by the command

```bash
> pip3 install numpy scipy matplotlib pandas
```

## Usage

The general structure is divided in two parts:

- "codes": Python files that define different tasks, like doing a single minimization or sweeping etc.
- "calculations": folders which include a file `in.yml` with concrete parameters. Every calculation uses one or more code. When the calculation is run, it outputs data files and plots in its folder.

The reason that the code is divided like this is to be able to tweak parameters quickly, and to make the choice of parameters for each result clear.

The calculations are run from the terminal using commands like this:

```bash
> python3 run.py <path to calculation folder>
```

So the calculation is run like this:

```bash
> python3 run.py <path to calculation folder>
```

## Running the calculations over SSH

I defined some useful commands to run calculations over SSH. Edit the file `bash_commands.sh` as follows:

```bash
SERVER_USERNAME=<your username here>
SERVER=<your server IP address here>
```

Then, in the terminal, write

```bash
> . bash_commands
```

to activate the commands. You can now in terminal write `calcinit` to prepare your remote computer, then write, e.g., `calcstart <path to calculation folder>` to run the phase diagram calculation. The results (when done) can be downloaded with `calcdown <path to calculation folder>`.
