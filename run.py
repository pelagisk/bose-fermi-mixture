"""Run scripts

Usage:
  run.py <calculation>

Options:
  -h --help     Show this screen.
  -d --debug    Run in debug mode.
  --version     Show version.

"""
from docopt import docopt
import yaml
import pandas as pd
import pprint

from git import Repo
repo = Repo(".")

import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '.')))

# import codes here
from codes.codes import *

pp = pprint.PrettyPrinter(indent=4)

if __name__ == '__main__':
    arguments = docopt(__doc__, version='1.0')
    path = arguments['<calculation>']

    with open(os.path.join(path, 'in.yml'), 'r') as f:
        calculation = yaml.safe_load(f.read())
    # except:
    #     calculation = {}

    pp.pprint(calculation)

    # tries to invoke the code which is specified
    code = str(calculation['code'])
    res = locals()[code](path=path, **calculation)
    # if isinstance(res, str):
    #     pass  # all is ok in this case
    # elif isinstance(res, pd.DataFrame):
    #     res = res.to_csv(index=False)
    # else:
    #     raise Error(f"No output method is set for type {type(res)}")
    # with open(os.path.join(path, 'out.dat'), 'w') as f:
    #     print(repo.head.commit.name_rev, file=f)
    #     print(res, file=f)

    # try:
    #
    # except KeyError:
    #     print("Did not find the code! Check spelling?")
