import numpy as np
import matplotlib.pyplot as plt
from scipy.special import factorial, hermite

def system(m=1, omega=1, hbar=1, N=20):

    c = (m * omega / np.pi * hbar)**(1/4)
    h_fns = [hermite(n) for n in range(N+2)]

    def psi(n, x):
        return c * h_fns[n](x) / np.sqrt(2**n * factorial(n)) * np.exp(-m * omega * x**2 / (2 * hbar))

    def n(x, y): 
        return sum(psi(n, x) * psi(n, y) for n in range(N))

    def n2(x, y):
        expr1 = 1
        if abs(x - y) > 1e-3:
            nominator = (h_fns[N-1](y) * h_fns[N](x) - h_fns[N](y) * h_fns[N-1](x))
            denominator = x - y
            expr1 = nominator / denominator
        expr2 = 1/(factorial(N) * 2**(N+1)) * expr1
        # return expr2
        return (c**2 * np.exp(-m * omega * (x**2 + y**2) / (2 * hbar))) * expr2
        
    return n2


# x_values = np.linspace(-8, 8, 400)
# n2 = system(m=1, omega=1, hbar=1, N=20)
# fig, ax = plt.subplots()
# n_data = np.zeros((len(x_values), len(x_values)))
# for a, x in enumerate(x_values):
#     for b, y in enumerate(x_values):
#         n_data[a, b] = n2(x, y)
# vmax = np.max(np.abs(n_data))
# pos = ax.imshow(n_data, cmap='RdBu', vmin=-vmax, vmax=vmax)
# plt.colorbar(pos, ax=ax)
# plt.show()

xmax = 10
omega_values = [1, 2, 4]
fig, ax = plt.subplots(2, len(omega_values), sharey=True)
for i, omega in enumerate(omega_values):
    
    nfn = system(m=1, omega=omega, hbar=1, N=20)

    # check the diagonal
    x_values = np.linspace(-xmax, xmax, 1000)
    n_data = np.zeros(len(x_values))
    for a, x in enumerate(x_values):
        n_data[a] = nfn(x, -x)
    ax[0, i].plot(x_values, np.log(np.abs(n_data)))
    ax[0, i].set_xlabel("x")
    ax[0, i].set_xlim(min(x_values), max(x_values))

    x_values = np.linspace(-xmax, xmax, 400)    
    n_data = np.zeros((len(x_values), len(x_values)))
    for a, x in enumerate(x_values):
        for b, y in enumerate(x_values):
            n_data[a, b] = nfn(x, y)
    vmax = np.max(np.abs(n_data))
    pos = ax[1, i].imshow(n_data[:, ::-1], cmap='RdBu', vmin=-vmax, vmax=vmax, 
        extent=(-xmax, xmax, -xmax, xmax)
    )
    ax[1, i].set_xlabel("x")
    ax[1, i].set_ylabel("y")
    # plt.colorbar(pos, ax=ax[1, i])
    ax[1, i].plot([-xmax, xmax], [xmax, -xmax], 'r-')

plt.tight_layout()
plt.show()
