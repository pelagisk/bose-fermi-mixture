tic
clear all, clc, %close all
%%%%% Imaginary time for effectoive boson mean-field model %%%%
k=2; %%% Factor scaling the trap, number of bosons, and fermion chemical potential %%%
omega=0.01/k;
%gbi=[0.1:0.05:1.5]; %%% g_b %%%
gbi=0.001
gbfi=0.0:0.02:0.2; %%% G_bf %%%
R=1; %%% Mass ratio %%%
mb=1/2;
mf=R*mb;
Nb=100*k; %%% Number of bosons %%%
Nf=96.5*k; %%% Number of fermions %%%
V0=0.1; %%% Optical lattice amplitude %%%

%%% Time grid %%%%%%%%%
dt=0.1;
nitt=1000; %2500;
tfinal=nitt*dt;
tint=dt:dt:tfinal;
%%%%%%%%%%%%%%%%%%%%%%%%

%%% x and p grids %%%%%%
%%% I use my own FFT which uses pre-set grids, x-grid should be symmetric
%%% %%%
ix=-400.5*pi; %%% Number of sites = 2*ix/pi %%%
fx=-ix;
N=1024*8; % N=20*801/2;
dx=fx/N;
x=[ix:dx:fx];
numb=length(x);
p=pi*(numb-1)*(0:(numb-1))/numb/max(x);
p=p-p(numb)/2;

disp("---------------------")
max(p)
disp("---------------------")
dp=p(2)-p(1);
f1=find(x>-pi);
mm=min(f1);
f1=find(x>0);
m0=min(f1);
f1=find(x>pi);
mp=min(f1);

%%%%%%%%%%%%%%%%%%%%%%%%%
Vpot=0.5*omega.^2*x.^2;
V=Vpot+V0*cos(x).^2;
Vp=p.^2/(2*mb);
alpha=(2*mf/pi^2);

Up=exp(-Vp*dt);

%%% Initial guesses %%%
psib0=sin(x/2).^2;
psib0=exp(-x.^2/1000);
psib0=psib0/sqrt(sum(abs(psib0).^2)*dx);
psif0=exp(-x.^2/1000);
psif0=psif0/sqrt(sum(abs(psif0).^2)*dx);
%%%%%%%%%%%%%%%%%%%%%%%

m=0;
for gb=gbi
    m=m+1;

n=0;
for gbf=gbfi
    n=n+1;
    [gb gbf]
    if n==1
        psib=psib0;
        psif=psif0;
    end
    [p,Psib]=fouriertrans(x,psib);


l=0;
for T=tint

    l=l+1;

    psib=exp(-(mb*Vpot+V0*cos(x).^2+2*gb*Nb*abs(psib).^2+gbf*Nf*abs(psif).^2)*dt).*psib;
    psib=psib/sqrt(sum(abs(psib).^2)*dx);

    [p,Psib]=fouriertrans(x,psib);
    Psib=Up.*Psib;
    Psib=Psib/sqrt(sum(abs(Psib).^2)*dp);

    [x,psib]=invfouriertrans(p,Psib);

    psif=exp(-(mf*Vpot+pi^2*Nf^2*abs(psif).^4/R+gbf*Nb*abs(psib).^2)*dt).*psif;
    psif=psif/sqrt(sum(abs(psif).^2)*dx);

end
L=max(x)-min(x);
ov(n,m)=Nb*Nf*real(sum(abs(psif).^2.*abs(psib).^2)*dx)/L; %%% density overlap %%%
Dxf(n,m)=sum(x.^2.*abs(psif).^2)*dx-(sum(x.*abs(psif).^2)*dx)^2; %%% Fermion width %%%
Dxb(n,m)=sum(x.^2.*abs(psib).^2)*dx-(sum(x.*abs(psib).^2)*dx)^2; %%% Boson density width %%%
end
end

toc

figure (1);
clf ();
plot(gbfi, ov);
print overlap2.jpg;

nb = Nb*abs(psib).^2
nf = Nf*abs(psif).^2

figure (2);
clf ();
hold on;
plot(x/pi, nb, 'b');
plot(x/pi, nf, 'r');
xlim([-400.5, 400.5]);
print densities2.jpg

csvwrite("nb2.csv", nb)
csvwrite("nf2.csv", nf)

csvwrite("jonas2.csv", ov)
