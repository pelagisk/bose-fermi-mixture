import numpy as np
import matplotlib.pyplot as plt


"""
We want to compare:

∫ dx (nf(x) nb(x))

"""

d = np.loadtxt('sweep.csv', delimiter=",", skiprows=2)
gbf_values = d[:, 0]
overlaps_axel = d[:, -1]

overlaps_jonas1 = np.loadtxt('jonas1.csv')

overlaps_jonas2 = np.loadtxt('jonas2.csv')

# they match approximately in shape, but not at all in magnitude

# overlaps_axel /= max(overlaps_axel)
# overlaps_jonas1 /= max(overlaps_jonas1)
# overlaps_jonas2 /= max(overlaps_jonas2)

plt.plot(gbf_values, overlaps_axel, label="Thomas-Fermi (Axels)")
plt.plot(gbf_values, overlaps_jonas1, label="Thomas-Fermi, eliminating fermions (Jonas)")
plt.plot(gbf_values, overlaps_jonas2, label="Thomas-Fermi (Jonas)")

gb = 0.001
nf = 0.0767
r = 1.0
gc = np.sqrt(4 * np.pi**2 * gb * nf / r)
plt.axvline(x=gc, color="red", label="Das (thermodynamic limit)")

plt.xlabel(r"$g_{bf}$")
plt.legend(loc='lower left')
plt.title(r"Overlap $\int n_b(x) n_f(x) \; dx$ for $g_b=0.001$")
plt.savefig("comparison.pdf")
plt.show()




nb1 = np.loadtxt('nb.csv', delimiter=",")
nf1 = np.loadtxt('nf.csv', delimiter=",")

nb2 = np.loadtxt('nb2.csv', delimiter=",")
nf2 = np.loadtxt('nf2.csv', delimiter=",")

x = np.linspace(-400.5, 400.5, len(nb1))

plt.plot(x, nb1, label="Thomas-Fermi (Jonas)")
plt.plot(x, nb2, label="Thomas-Fermi, eliminating fermions (Jonas)")
plt.legend(loc='lower right')
plt.title("Boson density, comparison")
plt.savefig("comparison_densities.pdf")
plt.show()
