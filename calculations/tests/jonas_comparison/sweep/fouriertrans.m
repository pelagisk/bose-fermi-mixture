function [p,F]=fouriertrans(x,f)

numb=length(x);
if numb/2==floor(numb/2)
    fprintf(1,'skit1')
    p=x-x;
    F=f-f;
    return
end

di=diff(x);
if min(di)-max(di)>1E-10
    fprintf(1,'skit2')
    p=x-x;
    F=f-f;
    return
end

if x(1)~=-x(numb)
    fprintf(1,'skit3')
    p=x-x;
    F=f-f;
    return
end

if di<=0
    fprintf(1,'skit4')
    p=x-x;
    F=f-f;
    return
end

dx=x(2)-x(1);

F=fftshift((dx/sqrt(2*pi))*exp(i*pi*(numb-1)/numb*(0:(numb-1))).*fft(f));
p=pi*(numb-1)*(0:(numb-1))/numb/max(x);
p=p-p(numb)/2;
