function [x,f]=invfouriertrans(p,F)

numb=length(p);
if numb/2==floor(numb/2)
    fprintf(1,'skit1')
    x=p-p;
    f=F-F;
    return
end

di=diff(p);
if min(di)-max(di)>1E-10
    fprintf(1,'skit2')
    x=p-p;
    f=F-F;
    return
end

if p(1)~=-p(numb)
    fprintf(1,'skit3')
    x=p-p;
    f=F-F;
    return
end

if di<=0
    fprintf(1,'skit4')
    x=p-p;
    f=F-F;
    return
end

dp=p(2)-p(1);

f=fftshift((dp/sqrt(2*pi))*exp(i*pi*(numb-1)/numb*(0:(numb-1))).*fft(F));
f=f(numb:-1:1);
x=pi*(numb-1)*(0:(numb-1))/numb/max(p);
x=x-x(numb)/2;
