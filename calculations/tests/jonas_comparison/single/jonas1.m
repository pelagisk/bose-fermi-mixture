tic
clear all, clc, %close all
%%%%% Imaginary time for effective boson mean-field model after eliminating fermions in TF %%%%
k=2; %%% Factor scaling the trap, number of bosons, and fermion chemical potential %%%
omega=0.01/k;
gbi=0.001
gbfi=0.1; %%% G_bf %%%
R=1; %%% Mass ratio %%%
mb=1/2;
mf=R*mb;
Nb=100*k; %%% Number of bosons %%%
mu=1*k; %%% Chemical potential %%%
V0=0.0; %%% Optical lattice amplitude %%%

%%% Time grid %%%%%%%%%
dt=0.01;
nitt=10000;
tfinal=nitt*dt;
tint=dt:dt:tfinal;
%%%%%%%%%%%%%%%%%%%%%%%%

%%% x and p grids %%%%%%
%%% I use my own FFT which uses pre-set grids, x-grid should be symmetric
%%% %%%
ix=-400.5*pi; %%% Number of sites = 2*ix/pi %%%
fx=-ix;
N=1024*8;
dx=fx/N;
x=[ix:dx:fx];
numb=length(x);
p=pi*(numb-1)*(0:(numb-1))/numb/max(x);
p=p-p(numb)/2;
dp=p(2)-p(1);
f1=find(x>-pi);
mm=min(f1);
f1=find(x>0);
m0=min(f1);
f1=find(x>pi);
mp=min(f1);

%%%%%%%%%%%%%%%%%%%%%%%%%
Vpot=0.5*omega.^2*x.^2;
Vol = V0*cos(x).^2;
V=Vpot+Vol;
Vp=p.^2/(2*mb);
alpha=(2*mf/pi^2);

Up=exp(-Vp*dt);

%%% Initial guesses %%%
psi0=sin(x/2).^2;
psi0=exp(-x.^2/1000);
psi0=psi0/sqrt(sum(abs(psi0).^2)*dx);

%%%%%%%%%%%%%%%%%%%%%%%

m=0;
for gb=gbi
    m=m+1;

n=0;
for gbf=gbfi
    n=n+1;
    [gb gbf]
    if n==1
        psi=psi0;
    end
    [p,Psi]=fouriertrans(x,psi);


l=0;
for T=tint

    nf=real(sqrt(alpha*(mu-R*Vpot-gbf*Nb*abs(psi).^2))); %%% Thomas-Fermi fermion density %%%

    l=l+1;

    Vx = mb*Vpot + Vol + 2*gb*Nb*abs(psi).^2 + gbf*nf;

    psi=exp(-Vx*dt).*psi;
    psi=psi/sqrt(sum(abs(psi).^2)*dx);

    [p,Psi]=fouriertrans(x,psi);
    Psi=Up.*Psi;
    Psi=Psi/sqrt(sum(abs(Psi).^2)*dp);

    [x,psi]=invfouriertrans(p,Psi);

    nf=sqrt(alpha*(mu-mf*Vpot-gbf*Nb*abs(psi).^2));
    Nf(l)=sum(nf)*dx; %%% Total number of fermions %%%

end
L=max(x)-min(x);
ov(n,m)=Nb*real(sum(nf.*abs(psi).^2)*dx)/L; %%% density overlap %%%

dnf=real(sqrt(alpha*(mu-Vpot-gbf*Nb*abs(psi).^2)))/(sum(real(sqrt(alpha*(mu-Vpot-gbf*Nb*abs(psi).^2))))*dx); %%% Normalized fermion density %%%
Dxf(n,m)=sum(x.^2.*dnf)*dx-(sum(x.*dnf)*dx)^2; %%% Fermion width %%%
Dx(n,m)=sum(x.^2.*abs(psi).^2)*dx-(sum(x.*abs(psi).^2)*dx)^2; %%% Boson density width %%%
nt(n,m)=Nf(l); %%% Number of fermions, I used to plot imag(nt) to spot the transition - it gives the clearest transition %%%
end
end

toc

ov

nb = Nb*abs(psi).^2;
nf = real(sqrt(alpha*(mu-R*Vpot-gbfi*nb))).^2;

csvwrite("nb1.csv", nb)
csvwrite("nf1.csv", nf)
