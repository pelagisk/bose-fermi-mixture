import numpy as np
import matplotlib.pyplot as plt

nb1 = np.loadtxt('nb1.csv', delimiter=",")
nf1 = np.loadtxt('nf1.csv', delimiter=",")
x = np.linspace(-400.5, 400.5, len(nb1)+1)[:-1]
# plt.plot(x, nb1/max(nb1), label=r"$n_b$, TF, eliminating fermions (Jonas)")
plt.plot(x, nf1/max(nf1), label=r"$n_f$, TF, eliminating fermions (Jonas)")

nb2 = np.loadtxt('nb2.csv', delimiter=",")
nf2 = np.loadtxt('nf2.csv', delimiter=",")
trap2 = np.loadtxt('trap2.csv', delimiter=",")
x = np.linspace(-400.5, 400.5, len(nb2)+1)[:-1]
# plt.plot(x, trap2 / np.pi**2, label=r"$V$, TF (Jonas)")
# plt.plot(x, nb2/max(nb2), label=r"$n_b$, TF (Jonas)")
plt.plot(x, nf2/max(nf2), label=r"$n_f$, TF (Jonas)")

nb3 = np.loadtxt('nb3.csv', delimiter=",")
nf3 = np.loadtxt('nf3.csv', delimiter=",")
trap3 = np.loadtxt('trap3.csv', delimiter=",")
x = np.linspace(-400.5, 400.5, len(nb3)+1)[:-1]
# plt.plot(x, trap3 / np.pi**2, label=r"$V$, TF (Jonas, new)")
# plt.plot(x, nb3/max(nb3), label=r"$n_b$, TF (Jonas, new)")
plt.plot(x, nf3/max(nf3), label=r"$n_f$, TF (Jonas, new)")

nb = np.loadtxt('nb.csv', delimiter=",")
nf = np.loadtxt('nf.csv', delimiter=",")
trap = np.loadtxt('trap.csv', delimiter=",")
x = np.linspace(-400.5, 400.5, len(nb)+1)[:-1]
# plt.plot(x, trap, label=r"$V$, TF (Axel)")
# plt.plot(x, nb/max(nb), label=r"$n_b$, TF (Axel)")
plt.plot(x, nf/max(nf), label=r"$n_f$, TF (Axel)")


plt.xlabel("$x / a$")
# plt.xlim(-12, 12)
# plt.xlim(-130, 130)
plt.legend(loc='lower right')
plt.title("Densities, comparison")
plt.savefig("comparison_densities.pdf")
plt.show()
