tic
clear all, clc, %close all
%%%%% Imaginary time for effectoive boson mean-field model %%%%
k=2; %%% Factor scaling the trap, number of bosons, and fermion chemical potential %%%
omega=0.01/k;
gbi=0.001%[0.1:0.05:1.5]; %%% g_b %%%
gbfi=0.1;%[0:0.025:1.2]; %%% G_bf %%%
R=1; %%% Mass ratio %%%
mb=1/2;
mf=R*mb;
Nb=100*k; %%% Number of bosons %%%
Nf=96.5*k; %%% Number of fermions %%%
V0=0.0; %%% Optical lattice amplitude %%%
old=0; %%% old=1 for old FFT %%%

%%% Time grid %%%%%%%%%
dt=0.01;
nitt=10000;
tfinal=nitt*dt;
tint=dt:dt:tfinal;
%%%%%%%%%%%%%%%%%%%%%%%%

%%% x and p grids %%%%%%
%%% I use my own FFT which uses pre-set grids, x-grid should be symmetric
%%% %%%
ix=-400.5*pi; %%% Number of sites = 2*ix/pi %%%
fx=-ix;
N=1024*8;
dx=fx/N;
x=[ix:dx:fx];
numb=length(x);
p=pi*(numb-1)*(0:(numb-1))/numb/max(x);
p=p-p(numb)/2;
dp=p(2)-p(1);
f1=find(x>-pi);
mm=min(f1);
f1=find(x>0);
m0=min(f1);
f1=find(x>pi);
mp=min(f1);

%%%%%%%%%%%%%%%%%%%%%%%%%
Vpot=0.5*omega.^2*x.^2;
Vol = V0*cos(x).^2;
V=Vpot+Vol;
Vp=p.^2/(2*mb);
alpha=(2*mf/pi^2);
Up=exp(-Vp*dt);
Vpf=p.^2/(2*1.0e10);
Uf=exp(-Vpf*dt);

%%% Initial guesses %%%
psib0=sin(x/2).^2;
psib0=exp(-x.^2/1000);
psib0=psib0/sqrt(sum(abs(psib0).^2)*dx);
psif0=exp(-x.^2/1000);
psif0=psif0/sqrt(sum(abs(psif0).^2)*dx);
%%%%%%%%%%%%%%%%%%%%%%%

m=0;
for gb=gbi
    m=m+1;

n=0;
for gbf=gbfi
    n=n+1;
    [gb gbf]
    if n==1
        psib=psib0;
        psif=psif0;
    end
    pf=psif;
    pb=psib;
    [p,Psib]=fouriertrans(x,psib);


l=0;
for T=tint

    l=l+1;

    Vbx = mb*Vpot + Vol + 2*gb*Nb*abs(psib).^2 + gbf*Nf*abs(psif).^2;
    psib=exp(-Vbx*dt).*psib;
    psib=psib/sqrt(sum(abs(psib).^2)*dx);

    Vfx = mf*Vpot + (pi^2*Nf^2*abs(psif).^4)/(2*mf) + gbf*Nb*abs(psib).^2;
    psif=exp(-Vfx*dt).*psif;
    psif=psif/sqrt(sum(abs(psif).^2)*dx);

    if old==1
        [p,Psib]=fouriertrans(x,psib);
        Psib=Up.*Psib;
        Psib=Psib/sqrt(sum(abs(Psib).^2)*dp);
        [x,psib]=invfouriertrans(p,Psib);
    else
        Psib=fftshift(fft(psib));
        Psib=Up.*Psib;
        Psib=Psib/sqrt(sum(abs(Psib).^2)*dp);
        psib=ifft(ifftshift(Psib));
        psib=psib/sqrt(sum(abs(psib).^2)*dx);
    end

    Psif=fftshift(fft(psif));
    Psif=Uf.*Psif;
    Psif=Psif/sqrt(sum(abs(Psif).^2)*dp);
    psif=ifft(ifftshift(Psif));
    psif=psif/sqrt(sum(abs(psif).^2)*dx);

    of(l)=abs(sum(pf.*psif));
    ob(l)=abs(sum(pb.*psib));
    pf=psif;
    pb=psib;

end
L=max(x)-min(x);
ov(n,m)=Nb*Nf*real(sum(abs(psif).*abs(psib).^2)*dx)/L; %%% density overlap %%%
Dxf(n,m)=sum(x.^2.*abs(psif).^2)*dx-(sum(x.*abs(psif).^2)*dx)^2; %%% Fermion width %%%
Dxb(n,m)=sum(x.^2.*abs(psib).^2)*dx-(sum(x.*abs(psib).^2)*dx)^2; %%% Boson density width %%%
end
end

toc


nb = Nb*abs(psib).^2;
nf = Nf*abs(psif).^2;

csvwrite("nb3.csv", nb)
csvwrite("nf3.csv", nf)
csvwrite("trap3.csv", Vpot)
