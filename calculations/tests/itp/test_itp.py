import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, ifft, fftfreq, fftshift, ifftshift

import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..')))

from codes.split_operator import *

N = 2**7
L = 16
x = np.linspace(-L/2, L/2, N+1)[:-1]
dx = abs(x[1] - x[0])
k = fftshift(fftfreq(N, d=dx)) * (2*np.pi)

K = 4*np.pi
psi = np.cos(K*x) + 0j
psi /= np.linalg.norm(psi)
plt.plot(x, np.abs(psi)**2)
plt.xlabel("x")
plt.show()
psi_k = fftshift(fft(psi))
plt.plot(k, np.abs(psi_k)**2)
plt.axvline(x=K)
plt.xlabel("k")
plt.show()


# psi_k = np.zeros(N) + 0j
# psi_k[N//2] = 1
# plt.plot(k, np.abs(psi_k)**2)
# plt.xlabel("k")
# plt.show()
# psi = ifft(ifftshift(psi_k))
# plt.plot(x, np.abs(psi)**2)
# plt.xlabel("x")
# plt.show()
#
# psi_k = np.zeros(N) + 0j
# psi_k[N//2-4] = psi_k[N//2+4] = 1
# plt.plot(k, np.abs(psi_k)**2)
# plt.xlabel("k")
# plt.show()
# psi = ifft(ifftshift(psi_k))
# plt.plot(x, np.abs(psi)**2)
# plt.xlabel("x")
# plt.show()

# def v(t, x, psi):
#     return 0
#
# def energy(x, psi):
#     return 0
#
#
# x = np.linspace(-10, 10)
# dx = abs(x[1] - x[0])
# psi = np.exp(-(x/0.1)**2) + 0j
# psi /= np.linalg.norm(psi)
#
# psi2, es = imaginary_time_propagate(x, psi, v, energy, n=100)
#
# print("Norm after is: ", np.real(dx * np.sum(psi*np.conj(psi))))
#
# overlap = np.real(dx * np.sum(psi*np.conj(psi2)))
#
# print("Overlap before/after: ", overlap)
#
# plt.plot(es)
# plt.show()
