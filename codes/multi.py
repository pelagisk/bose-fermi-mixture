import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from git import Repo

from codes.helpers import *
from codes.plot import *
from codes.single import *
from codes.sweep import *


plt.style.use('style.mplstyle')


def multi(plot=True, header='', row='', id_label='id', legend=True, **calculation):
    """
    """

    each = calculation['each']
    each['path'] = calculation['path']
    observables = calculation['observables']
    calculations = calculation['calculations']

    # if an id key is present, use that. Otherwise enumerate using numbers
    if not all(['id' in sub for sub in calculations]):
        for i, sub in enumerate(calculations):
            sub['id'] = f"{i:04d}"

    if plot == True:

        figs, ax = {}, {}
        for observable in observables:
            figs[observable], ax[observable] = plt.subplots()

    # make sure the plots know where the params are located
    if 'plots' in calculation['each']['each']:
        for plot in calculation['each']['each']['plots']:
            plot['base_params_path'] = "../"

    for sub in calculations:

        # prepare subdirectory to sub
        sub['path'] = os.path.join(calculation['path'], sub['id'])
        prepare_path(sub['path'])

        # use the label for latex label if not provided
        if not 'latex' in sub:
            sub['latex'] = sub['label']

        # update each with the relevant parameters
        update_nested(each, sub)

        # run calculation
        if each['code'] == 'sweep':
            sweep(
                header = (header + f'{id_label},'),
                row = (row + f'{sub["id"]},'),
                # plot = False,
                **each
            )

            # plotting
            if plot == True:
                each['path'] = calculation['path']
                data_path = sub['id']
                plot_sweep(
                    ax = ax,
                    data_path = data_path,
                    plot_label = latex_string(sub['latex']),
                    **each
                )
        else:
            raise NotImplementedError()

    # concatenating data files
    dfs = []
    for sub in calculations:
        df = pd.read_csv(
            os.path.join(sub['path'], f"sweep.csv"), header=1, index_col=False)
        dfs.append(df)

    df = pd.concat(dfs, ignore_index=True)
    data_path = os.path.join(calculation['path'], 'multi.csv')
    file = open(data_path, 'w')
    repo = Repo(".")
    print(repo.head.commit.name_rev, file=file)
    print(df.to_csv(index=False), end="", file=file)
    file.close()

    if 'compare_das' in calculation and calculation['compare_das'] == True:
        for observable in observables:
            plot_compare_das(df, ax[observable], legend=True, **each)

    if plot == True:
        for observable in observables:
            if legend == True:
                ax[observable].legend()
            o_path = os.path.join(calculation['path'], f"{observable}.pdf")
            figs[observable].savefig(o_path)
