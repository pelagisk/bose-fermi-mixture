import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
import matplotlib.pyplot as plt

from codes.helpers import *
from codes.thomas_fermi import *
from codes.born_oppenheimer import *
from codes.nfea import nfea


plt.style.use('style.mplstyle')


def single(method='tf_itp', L=100, **calculation):
    """
    A single run
    """

    # params = calculation['mixture_params']

    # set scaled parameters (useful for thermodynamic limit)
    # note: all lengths measured in units of optical lattice

    #omega_scaling='linear',

    # if not ('omega_relative' in params):
    #     calculation['mixture_params']['omega_relative'] = 0.0
    # if omega_scaling == 'linear':
    #     calculation['mixture_params']['omega'] = params['omega_relative'] / L
    # elif omega_scaling == 'quadratic':
    #     calculation['mixture_params']['omega'] = params['omega_relative'] / L**2
    # elif omega_scaling == 'constant':
    #     calculation['mixture_params']['omega'] = params['omega_relative']
    # else:
    #     raise NotImplementedError(
    #         f"The scaling '{omega_scaling}' is not recognized!"
    #     )

    calculation['mixture_params'] = set_densities(
        L=L, **calculation['mixture_params']
    )
    L = int(L)

    if method == 'tf_itp':
        return single_tf_itp(L=L, **calculation)
    elif method == 'bo_min':
        return single_bo_min(L=L, **calculation)
    elif method == 'bo_itp':
        return single_bo_itp(L=L, **calculation)
    elif method == 'nfea':
        return nfea(**calculation)
    else:
        raise NotImplementedError(f"Method {method} is not implemented.")
