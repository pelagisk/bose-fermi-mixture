import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import yaml
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from codes.helpers import *
from codes.born_oppenheimer import *
from codes.fig2 import *


plt.style.use('style.mplstyle')


def gap_thesis(**calculation):

    fig, ax = plt.subplots(2, 1)

    # 2c. KT fits
    fig2c(ax=ax[0], path=calculation['path'], **calculation['kt'])
    
    # 2d. densities
    plot_densities(ax=ax[1], **{**calculation, **calculation['densities']})

    fig.tight_layout()
    fig.savefig(os.path.join(calculation['path'], f"gap.pdf"))


def phase_diagram_thesis(**calculation):

    fig, ax = plt.subplots(2, 1)

    # 2a. phase diagram in (gbf, gb)

    # plot_gap(ax = ax[0], **{**calculation, **calculation['fig2a']['gap']})
    plot_kt_transition(ax = ax[0], **{**calculation, **calculation['fig2a']['kt']})

    for c in calculation['fig2a']['first_order']:
        plot_first_order(ax = ax[0], **{**calculation, **c})

    for a in calculation['fig2a']['annotations']:
        # TODO fixme
        x, y = a['xy']
        a['xy'] = x*4, y*4
        ax[0].annotate(**a)

    # 2b. phase diagram in (gbf, V0)

    # plot_gap(ax = ax[1], **{**calculation, **calculation['fig2b']['gap']})
    plot_kt_transition(ax = ax[1], **{**calculation, **calculation['fig2b']['kt']})

    for c in calculation['fig2b']['first_order']:
        plot_first_order(ax = ax[1], **{**calculation, **c})

    for a in calculation['fig2b']['annotations']:
        # TODO fixme
        x, y = a['xy']
        a['xy'] = x*4, y*4
        ax[1].annotate(**a)


    fig.tight_layout()
    fig.savefig(os.path.join(calculation['path'], f"phase_diagram.pdf"))
