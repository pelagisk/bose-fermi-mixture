import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from git import Repo

from codes.helpers import *
from codes.single import *
from codes.plot import *


plt.style.use('style.mplstyle')


def sweep(direction='forward', plot=True, g_label='gbf', **calculation):
    """
    Sweeps over several runs. Parameters are specified in calculation as

    sweep:
        variable: "gbf" (variable name)
        latex: "g_{bf}" (or other latex string for plot axes etc)
        start: _
        stop: _
        steps: _
        reuse_wavefunction: True/False
        direction: "forward"/"backward"/"both"

    """

    # when sweeping in both directions, save separate data files and then select
    # the state of lowest energy from both sweeps, for each point in the sweep
    if direction == 'both':

        path = calculation['path']

        # make sure the plots know where the params are located
        if 'plots' in calculation['each']:
            for plot_ in calculation['each']['plots']:
                if 'base_params_path' in plot_:
                    plot_['params_path'] = os.path.join(
                        plot_['base_params_path'], "../../../"
                    )
                else:
                    plot_['params_path'] = "../../../"

        # TODO put these two in loop instead

        calculation['path'] = os.path.join(path, 'forward')
        sweep_aux(direction='forward', **calculation)
        df_forward = pd.read_csv(
            os.path.join(calculation['path'], 'sweep.csv'), header=1)
        df_forward = df_forward.sort_values(by=g_label)
        df_forward = df_forward.reset_index(drop=True)

        calculation['path'] = os.path.join(path, 'reverse')
        sweep_aux(direction='reverse', **calculation)
        df_reverse = pd.read_csv(
            os.path.join(calculation['path'], 'sweep.csv'), header=1)
        df_reverse = df_reverse.sort_values(by=g_label)
        df_reverse = df_reverse.reset_index(drop=True)

        calculation['path'] = path

        condition = df_forward['energy'] <= df_reverse['energy']
        df = df_forward.where(condition, other=df_reverse)

        file = open(os.path.join(calculation['path'], 'sweep.csv'), 'w')
        repo = Repo(".")
        print(repo.head.commit.name_rev, file=file)
        print(df.to_csv(index=False), end="", file=file)
        file.close()

    else:

        # make sure the plots know where the params are located
        if 'plots' in calculation['each']:
            for plot_ in calculation['each']['plots']:
                if 'base_params_path' in plot_:
                    plot_['params_path'] = os.path.join(
                        plot_['base_params_path'], "../../"
                    )
                else:
                    plot_['params_path'] = "../../"

        sweep_aux(direction=direction, g_label=g_label, **calculation)

    if plot == True:
        plot_sweep(direction=direction, g_label=g_label, **calculation)


def sweep_aux(header='', row='', values=None, g_label='gbf', g_latex='g_{bf}',
              location=['mixture_params'], direction='forward', to_print=[],
              reuse_wavefunction=False, logging=True, **calculation):
    """Used internally to sweep a single time. See docs for `sweep`.
    """

    g_values = get_sweep_values(
        values = values,
        params = calculation['each']['mixture_params'],
        x = g_label,
    )

    if direction == 'reverse':
        g_values = g_values[::-1]

    # prepare directory to save iteration plots
    iterations_dir = os.path.join(calculation['path'], f"iterations")
    prepare_path(iterations_dir)

    # prepare stream to output file and terminal
    file = FileAndTerminal(
        file = open(os.path.join(calculation['path'], 'sweep.csv'), 'w'),
        logging = logging
    )

    # print header
    repo = Repo(".")
    print(repo.head.commit.name_rev, file=file)
    print(header, end="", file=file)
    print(f"{g_label},", end="", file=file)
    print(",".join(to_print), file=file)

    each = calculation['each']

    # start from a specified initial state
    psi = None

    for (j, g) in enumerate(g_values):

        # print sweeping/looping parameters
        print(row, end="", file=file)

        # TODO weird bug when columns are rounded
        print(f"{g:.12f},", end="", file=file)

        # prepare directory for this iteration
        iteration_path = os.path.join(iterations_dir, f'{j:04d}')
        prepare_path(iteration_path)

        # single run with modified parameters
        update_nested_by_path(each, location + [g_label], g)
        each.update(dict(
            psi = psi,
            file = file,
            path = iteration_path,
            to_print = to_print,
        ))

        # run the appropriate codes
        if each['code'] == 'single':
            psi = single(**each)
        # elif each['code'] == 'sweep':
        #     psi = sweep(**each)
        else:
            raise NotImplementedError(f"Code {each['code']} not implemented")

        print("", file=file)

        # continue finding the state from scratch for each iteration?
        if reuse_wavefunction == False:
            psi = None

    file.close()

    return psi


def plot_sweep(ax=None, plot_label=None,
               g_label='gbf', g_latex='g_{bf}', to_print=[],
               observables_latex=None, **calculation):

    calculation = set_params_and_data_path(**calculation)

    each = calculation['each']

    observables = to_print
    # just use string as latex string if its not provided
    if observables_latex is None:
        observables_latex = observables

    df = pd.read_csv(os.path.join(calculation['data_path'], 'sweep.csv'), header=1, index_col=False)

    for (observable, latex) in zip(observables, observables_latex):

        if ax is None:
            fig, ax_ = plt.subplots()
        else:
            # TODO does this work?
            if observable not in ax:
                continue
            ax_ = ax[observable]

        plot_observable(
            df,
            ax_,
            x = g_label,
            x_latex = g_latex,
            y = observable,
            y_latex = latex,
            plot_label = plot_label,
            **calculation,
        )

        if 'compare_das' in calculation and calculation['compare_das'] == True:
            plot_compare_das(df, ax_, g_label=g_label, **calculation)

        if ax is None:
            fig.savefig(os.path.join(calculation['path'], f"{observable}.pdf"))


def get_sweep_values(values=None, x='gb', params={}):
    if isinstance(values, list):
        return values
    elif 'linspace' in values:
        return np.linspace(**values['linspace'])
    elif 'follow_das' in values:
        return follow_das(x=x, params=params, **values['follow_das'])
    elif 'follow_das_v0' in values:
        return follow_das_v0(x=x, params=params, **values['follow_das_v0'])
    else:
        raise ValueError("Sweep values not properly specified")


def follow_das(constant=1, sign=1, num=3, x='gb', params={}):
    xc = find_das(x=x, **params)
    return np.linspace(0, sign * (xc + constant), num)


def find_das(x='gbf', **params):
    if 'nuf' in params:
        params['nf'] = params['nuf'] / (2 * np.pi)
    if x == 'gb':
        xc = params['gbf']**2 / (2 * np.pi**2 * params['nf'] / params['r'])
    elif x == 'gbf':
        xc = np.sqrt(params['gb'] * (2 * np.pi**2 * params['nf'] / params['r']))
    else:
        raise ValueError(f"Can't predict critical value of variable {x}")
    return xc


def follow_das_v0(constant=1, sign=1, num=3, x='gb', rate=0.5, g_label='v0',
                  params={}):
    xc = find_das(x=x, **params)
    y_value = params[g_label]
    return np.linspace(0, sign * (xc + rate * y_value + constant), num)
