import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import yaml
import numpy as np
from scipy.integrate import quad
import scipy.optimize as opt
from scipy.sparse.linalg import eigsh
import matplotlib.pyplot as plt

from codes.helpers import *
from codes.plot import *
from codes.split_operator import imaginary_time_propagate


class Hybrid(object):

    def __init__(self, v0=0, r=0.04, gb=0.1, gbf=0.01, **kwargs):
        self.r = r
        self.v0 = v0
        self.gb = gb
        self.gbf = gbf

    def integrate(self, x, f):
        dx = abs(x[1] - x[0])
        return dx * np.sum(f)

    def differentiate(self, x, f):
        dx = abs(x[1] - x[0])
        # TODO take diff into account?
        return (np.roll(f, 1) - f) / dx

    def boson_density(self, x, psi):
        pass

    def boson_potential(self, x):
        pass

    def boson_energy(self, x, psi):
        pass

    def fermion_density(self, x, psi, nq=101):
        pass

    def fermion_potential(self, x):
        pass

    def fermion_energy(self, x, psi):
        pass

    def gpe_potential(self, t, x, psi, nq=101):
        nf = self.fermion_density(x, psi, nq=nq)
        return self.gpe_potential_bosons(t, x, psi) + self.gbf * nf

    def gpe_potential_bosons(self, t, x, psi):
        v = self.boson_potential(x)
        nb = self.boson_density(x, psi)
        return v + self.gb * nb

    def total_energy(self, x, psi):
        return self.boson_energy(x, psi) + self.fermion_energy(x, psi)


class HybridFinite(Hybrid):

    def __init__(self, Nf=2, Nb=100, omega_b=0, omega_f=0, wall=0, **kwargs):
        self.omega_b = omega_b
        self.omega_f = omega_f
        self.wall = wall
        self.Nf = int(Nf)
        self.Nb = Nb
        super().__init__(**kwargs)

    def boson_density(self, x, psi):
        nrm = self.integrate(x, np.abs(psi)**2)
        psi /= nrm
        return self.Nb * np.abs(psi)**2

    def boson_potential(self, x):
        v = self.v0 * np.cos(x / 2)**2
        mb = 1 / 2
        v += (1/2) * mb * self.omega_b**2 * x**2
        v[0] += self.wall
        v[-1] += self.wall
        return v

    def boson_energy(self, x, psi):
        ek = self.boson_kinetic_energy(x, psi)
        euv = self.boson_potential_energy(x, psi)
        return ek + euv

    def boson_kinetic_energy(self, x, psi):
        nrm = self.integrate(x, np.abs(psi)**2)
        psi /= nrm
        dpsi = self.differentiate(x, psi)
        ek = self.integrate(x, self.Nb * np.abs(dpsi)**2)
        return np.real(ek)

    def boson_potential_energy(self, x, psi):
        v = self.boson_potential(x)
        ev = self.integrate(x, v * self.Nb * np.abs(psi)**2)
        eu = self.integrate(x, (1/2) * self.gb * self.Nb**2 * np.abs(psi)**4)
        return np.real(ev + eu)

    def fermion_density(self, x, psi, **kwargs):
        dx = abs(x[1] - x[0])
        N = len(x)
        es, vs = np.linalg.eigh(self.fermion_hamiltonian(x, psi))
        n = np.zeros(N)
        for i in range(int(self.Nf)):
            n += np.abs(vs[:, i])**2 / dx
        return n

    def fermion_potential(self, x, nb):
        mf = self.r / 2
        v = (1/2) * mf * self.omega_f**2 * x**2
        v[0] += self.wall
        v[-1] += self.wall
        v += self.gbf * nb
        return v

    def fermion_energy(self, x, psi):
        es = np.linalg.eigvalsh(self.fermion_hamiltonian(x, psi))
        return sum(es[:self.Nf])

    def fermion_hamiltonian(self, x, psi):
        dx = abs(x[1] - x[0])
        N = len(x)
        nb = self.boson_density(x, psi)
        v = self.fermion_potential(x, nb)
        t = 1 / (self.r * dx**2)
        h = np.diag(v + 2 * t, k=0) + \
            np.diag(-t * np.ones(N - 1), k=1) + \
            np.diag(-t * np.ones(N - 1), k=-1) + 0j
        return h

    def fermion_diagonalization(self, x, psi):
        return np.linalg.eigh(self.fermion_hamiltonian(x, psi))

    def correlator(self, x, psi):
        print("Finite correlator")
        c = np.zeros((len(x), len(x))) + 0.0j
        e, v = self.fermion_diagonalization(x, psi)
        for i in range(self.Nf):
            c += np.einsum('i,j->ij', v[:, i].conjugate(), v[:, i])
        return c

    def tof_fermion(self, x, psi, n_virtual=1):
        intensity = np.zeros(len(x) * n_virtual)
        e, v = self.fermion_diagonalization(x, psi)
        for i in range(self.Nf):
            k, vik = fourier_transform(x, v[:, i], n_virtual=n_virtual)
            intensity += np.abs(vik)**2
        return k, intensity


class HybridInfinite(Hybrid):

    def __init__(self, nf=0.5, nb=10, epsrel=1e-1, lanczos=False, trunc=8,
                 **kwargs):
        self.nf = nf
        self.nb = nb
        self.epsrel = epsrel
        self.lanczos = lanczos
        self.trunc = trunc
        super().__init__(**kwargs)

    def boson_density(self, x, psi):

        # TODO norm not always preserved!
        nrm = self.integrate(x, np.abs(psi)**2)
        # if abs(nrm - 1) > 1e-5:
        #     print(f"Warning: norm - 1: {nrm - 1}")
            # raise AssertionError(f"norm - 1: {nrm - 1}")
        psi /= nrm

        dx = abs(x[1] - x[0])
        L = abs(x[-1] - x[0]) + dx  # endpoint must be included
        # print(self.nb * L * self.integrate(x, np.abs(psi)**2))
        return self.nb * np.abs(psi)**2

    def boson_potential(self, x):
        return self.v0 * np.cos(x / 2)**2

    def boson_energy(self, x, psi):
        ek = self.boson_kinetic_energy(x, psi)
        euv = self.boson_potential_energy(x, psi)
        return ek + euv

    def boson_kinetic_energy(self, x, psi):
        nrm = self.integrate(x, np.abs(psi)**2)
        psi /= nrm
        dpsi = self.differentiate(x, psi)
        ek = self.integrate(x, self.nb * np.abs(dpsi)**2)
        return np.real(ek)

    def boson_potential_energy(self, x, psi):
        v = self.boson_potential(x)
        ev = self.integrate(x, v * self.nb * np.abs(psi)**2)
        eu = self.integrate(x, (1/2) * self.gb * self.nb**2 * np.abs(psi)**4)
        return np.real(ev + eu)

    def fermion_density(self, x, psi, nq=101):
        _, nf, _ = self.fermion_density_and_spectrum(x, psi, nq=nq)
        return nf

    def band_data(self):
        nuf = (2 * np.pi) * self.nf
        kf = (nuf / 2) % 0.5
        n_bands = int((nuf / 2) / 0.5) + 1
        return nuf, kf, n_bands

    def fermion_density_and_spectrum(self, x, psi, nq=101, plot=False):
        nb = self.boson_density(x, psi)
        vx = self.fermion_potential(x, nb)
        k, vk = fourier_transform(x, vx)

        nuf, kf, n_bands = self.band_data()

        q_values = np.linspace(-1/2, 1/2, nq)
        dq = abs(q_values[1] - q_values[0])

        # k2, vk2 = self.truncate(k, vk)

        nk = len(k)
        spectrum = np.zeros((nq, nk))
        v_values = np.zeros((nq, nk, nk)) + 0.0j
        nf = np.zeros(len(psi)) + 0.0j

        for (i, q) in enumerate(q_values):
            e, v = self.fermion_diagonalization(q, k, vk)
            spectrum[i, :] = e
            v_values[i, :, :] = v

        _, v_values_x = fourier_transform(k, v_values, axis=1)

        for (i, qi) in enumerate(q_values):

            for j in range(0, n_bands - 1):
                psixq = v_values_x[i, :, j]
                nf += dq * np.abs(psixq)**2

            # last band filled up only to kf
            if -kf <= qi <= kf:
                psixq = v_values_x[i, :, n_bands - 1]
                nf += dq * np.abs(psixq)**2

        nf = abs(nf)

        # TODO fermions do not sum up to correct number
        # assert(abs(self.integrate(x, nf) - self.nf) < 1e-5)
        nf_integrated = self.integrate(x, nf)
        nf *= self.nf / nf_integrated

        return q_values, nf, spectrum


    def fermion_potential(self, x, nb):
        return self.gbf * nb

    def fermion_energy(self, x, psi):
        nb = self.boson_density(x, psi)
        vx = self.fermion_potential(x, nb)
        k, vk = fourier_transform(x, vx)

        nuf, kf, n_bands = self.band_data()

        def sum_n_lowest(q, n):
            return sum(self.fermion_bands(q, k, vk, n))

        # in interval [0, kf] sum all sublattice bands
        e = quad(sum_n_lowest, 0, kf, args=(n_bands,), epsrel=self.epsrel)[0]
        # in interval [kf, 1/2] sum every completely filled sublattice band
        if n_bands > 1:
            e += quad(sum_n_lowest, kf, 1/2, args=(n_bands - 1,),
                      epsrel=self.epsrel)[0]
        return 2 * e

    def fermion_bands(self, q, k, vk, n):
        if self.lanczos == True:
            return eigsh(self.fermion_hamiltonian(q, k, vk), k=n, which='SA',
                         return_eigenvectors=False)
        else:
            return np.linalg.eigvalsh(self.fermion_hamiltonian(q, k, vk))[0:n]

    def fermion_diagonalization(self, q, k, vk):
        return np.linalg.eigh(self.fermion_hamiltonian(q, k, vk))

    def truncate(self, k, vk):
        assert(len(k) % 2 == 0)
        k = k[1:]
        vk = vk[1:]
        mid = len(k) // 2
        if len(k) > 2 * self.trunc + 1:
            k = k[(mid - self.trunc):(mid + self.trunc + 1)]
            vk = vk[(mid - self.trunc):(mid + self.trunc + 1)]
        return k, vk

    def fermion_hamiltonian(self, q, k, vk):
        # add all bands from vk
        N = len(k)
        mid = len(k) // 2
        h = np.diag((1 / self.r) * (q + k)**2) + 0.0j
        for i, vki in enumerate(vk):
            pos = mid - i
            h += np.diag(np.ones(N - abs(pos)), k=pos) * vki
        return h

    def peierls_gap(self, x, psi, **kwargs):
        nb = self.boson_density(x, psi)
        vx = self.fermion_potential(x, nb)
        k, vk = fourier_transform(x, vx)
        nuf = 2 * np.pi * self.nf
        kf = (nuf/ 2) % 0.5
        n_bands = int((nuf / 2) / 0.5) + 2
        b = self.fermion_bands(kf, k, vk, n_bands)
        return b[-1] - b[-2]

    def correlator(self, x, psi, nq=101):
        c = np.zeros((len(x), len(x))) + 0.0j

        nb = self.boson_density(x, psi)
        vx = self.fermion_potential(x, nb)
        k, vk = fourier_transform(x, vx)

        nuf, kf, n_bands = self.band_data()

        q_values = np.linspace(-1/2, 1/2, nq)
        dq = abs(q_values[1] - q_values[0])

        # k2, vk2 = self.truncate(k, vk)

        nk = len(k)
        spectrum = np.zeros((nq, nk))
        v_values = np.zeros((nq, nk, nk)) + 0.0j
        nf = np.zeros(len(psi)) + 0.0j

        for (i, q) in enumerate(q_values):
            e, v = self.fermion_diagonalization(q, k, vk)
            spectrum[i, :] = e
            v_values[i, :, :] = v

        _, v_values_x = fourier_transform(k, v_values, axis=1)

        for (i, qi) in enumerate(q_values):

            for j in range(0, n_bands - 1):
                psixq = v_values_x[i, :, j]
                nf += dq * np.abs(psixq)**2
                c += dq * np.einsum('i,j->ij', psixq.conjugate(), psixq)

            # last band filled up only to kf
            if -kf <= qi <= kf:
                psixq = v_values_x[i, :, n_bands - 1]
                nf += dq * np.abs(psixq)**2
                c += dq * np.einsum('i,j->ij', psixq.conjugate(), psixq)

        return c


def initial_state_boa(initial_state='sin', ns=10, L=2, x0_relative=1.0, eta=0.1,
    r=1, **calculation):
    x = positions(L=L, ns=ns)
    x0 = x0_relative * (max(x) - min(x))
    if initial_state == 'file':
        psi = np.loadtxt(os.path.join(calculation['path'], 'psi.csv')) + 0j
    elif initial_state == 'gaussian':
        psi = np.exp(-(x/x0)**2) + 0j
    elif initial_state == 'gaussian_wiggly':
        psi = np.exp(-(x/x0)**2) * (1 + eta * np.sin(x / 2)**(2*r)) + 0j
    elif initial_state == 'sin':
        psi = np.sin(x / 2)**2 + 0j
    elif initial_state == 'sin_peierls':
        psi = np.sin(x / 2)**2 + eta * np.sin(x / 4)**2 + 0j
    else:
        psi = np.ones(len(x)) + 0j
    psi /= np.linalg.norm(psi)
    return x, psi


def minimize(energy, x, psi, method='fast', n=100, T=10):
    """Minimizes, i.e. finds the mean-field self-consistently, with respect
    to the total energy (or any other function of psi and the coupling
    constants of the problem)."""

    # constrains psi in energy(psi) using Lagrange multipliers
    dx = abs(x[1] - x[0])
    cons = ({
        'type': 'eq',
        'fun': lambda psi: 1 - dx * np.sum(np.abs(psi)**2),
    })

    psi = np.real(psi)
    fun = lambda psi: energy(x, psi)

    # The 'robust' method uses a combination of simulated annealing and
    # gradient descent. It can be used as a check of the 'fast' method.
    if method == 'robust':
        def callback(psi, energy, accept):
            print("energy:", energy, "accept:", accept)
        res = opt.basinhopping(fun, x0=psi, niter=n, T=T,
                           minimizer_kwargs=dict(constraints=cons),
                           callback=callback)

    # the 'fast' minimizer uses gradient descent
    elif method == 'fast':
        res = opt.minimize(fun, x0=psi, constraints=cons)
    # other methods are not implemented yet
    else:
        raise NotImplementedError("Methods: 'fast' or 'robust'")
    psi = res.x
    E = res.fun
    return E, psi


def single_bo_min(L=100, ns=10, psi=None, plot=False, bosons_only=False,
    old_code=False,
    anneal_lattice=False, finite=False, **calculation):

    params = calculation['mixture_params']
    if old_code == True:
        mixture = BornOppenheimer(**params)
    elif finite == True:
        mixture = HybridFinite(**params)
    else:
        mixture = HybridInfinite(**params)
    # mixture = BornOppenheimer(finite=finite, **params)

    x, psi_ = initial_state_boa(L=L, ns=ns, **calculation)
    if psi is None:
        psi = psi_

    min_params = calculation['min_params']

    if bosons_only == True:
        E, psi = minimize(mixture.boson_energy, x, psi, **min_params)
    else:
        E, psi = minimize(mixture.total_energy, x, psi, **min_params)

    return finish_single(x, psi, mixture, L=L, ns=ns, finite=finite,
                         bosons_only=bosons_only, **calculation)


def single_bo_itp(L=100, ns=10, nq=101, plots=[], psi=None, bosons_only=False,
                  finite=False, **calculation):

    params = calculation['mixture_params']
    if finite == True:
        mixture = HybridFinite(**params)
    else:
        mixture = HybridInfinite(**params)
    # mixture = BornOppenheimer(finite=finite, **params)

    x, psi_ = initial_state_boa(L=L, ns=ns, **calculation)
    if psi is None:
        psi = psi_

    # decide on the protocol - by default just a single run
    protocol = []
    if 'protocol' in calculation:
        protocol = calculation['protocol']
    else:
        protocol = [dict(bosons_only=bosons_only, **calculation)]

    # step through the protocol and save iteration data and wavefunction
    energy_values = []
    norm_values = []
    for step in protocol:

        itp_params = step['itp_params']

        if 'verbose' in itp_params and itp_params['verbose'] == True:
            callback = (lambda i, t, p, d: print(f"{i}: t={t:.10f}"))
        else:
            callback = None

        if step['bosons_only'] == False:
            energy = lambda x, psi: mixture.total_energy(x, psi[0])
            v = lambda t, x, psi: mixture.gpe_potential(t, x, psi[0], nq=nq)
        else:
            energy = lambda x, psi: mixture.boson_energy(x, psi[0])
            v = lambda t, x, psi: mixture.gpe_potential_bosons(t, x, psi[0])

        psi, es, ds = imaginary_time_propagate(x, psi, v, energy,
            m=0.5, callback=callback, **itp_params
        )
        psi = psi[0]
        energy_values.append(es)
        norm_values.append(ds)

    energy_values = np.concatenate(energy_values)
    norm_values = np.concatenate(norm_values)

    # save convergence plots
    np.savetxt(
        os.path.join(calculation['path'], "convergence_energy.csv"),
        energy_values
    )
    np.savetxt(
        os.path.join(calculation['path'], "convergence_norm.csv"),
        norm_values
    )

    return finish_single(x, psi, mixture, plots=plots, L=L, ns=ns,
                         bosons_only=bosons_only, finite=finite, nq=nq,
                         **calculation)


def compare_itp(protocols=[], startfrom=0, **calculation):
    fig, ax = plt.subplots(2)

    strings = []

    for protocol in protocols:
        path = os.path.join(calculation['path'], protocol['protocol_label'])
        prepare_path(path)
        c = {**calculation['each'], **protocol}
        c['psi'] = None
        c['path'] = path
        # c['data_path']
        # if 'initial_state' in c: print(c['initial_state'])
        # print(c)

        params = c['mixture_params']
        L = c['L']
        c['mixture_params'] = set_densities(
            L=L, **c['mixture_params']
        )
        L = int(L)

        single_bo_itp(**c)
        es = np.loadtxt(os.path.join(path, "convergence_energy.csv"))
        ds = np.loadtxt(os.path.join(path, "convergence_norm.csv"))
        its = range(len(es))[startfrom:]
        ax[0].plot(its, es[startfrom:], label=protocol['protocol_label'])
        ax[1].semilogy(its, ds[startfrom:], label=protocol['protocol_label'])

        strings.append(f"Protocol {protocol['protocol_label']} converged to: {es[-1]}")

    fig.suptitle("Convergence")
    ax[0].set_xlabel("Iteration")
    ax[0].set_ylabel("Energy")
    ax[0].legend()
    ax[1].set_xlabel("Iteration")
    ax[1].set_ylabel(r"$\log (1 - | \langle \psi | \psi' \rangle |^2)$")
    ax[1].legend()
    fig.tight_layout()
    fig.savefig(os.path.join(calculation['path'], "convergence.pdf"))
    plt.close(fig)


def finish_single(x, psi, mixture, plots=[], to_print=['energy', 'peierls_gap'],
                  **calculation):

    # save psi to file
    np.savetxt(os.path.join(calculation['path'], "psi.csv"), np.real(psi))

    # plot things
    for plot in plots:
        dispatch_plot(plot, **calculation)

    # print various measurables to output
    printout(x, psi, mixture, **calculation)

    return psi


def dispatch_plot(plot, **calculation):
    dict_of_plot_functions = dict(
        convergence = plot_convergence,
        densities = plot_densities,
        cumulative_fermion_density = plot_cumulative_fermion_density,
        fermion_density_per_site = plot_fermion_density_per_site,
        fourier_boson = plot_fourier_boson,
        correlator = plot_correlator,
        tof_fermion = plot_tof_fermion,
    )
    if plot['type'] in dict_of_plot_functions:
        plot_function = dict_of_plot_functions[plot['type']]
        # note: here, keywords of `plot` overrides those in `calculation`
        return plot_function(**{**plot, **calculation})


def printout(x, psi, mixture, to_print=['energy', 'peierls_gap'], L=2,
             bosons_only=False, finite=False, file=sys.stdout, **calculation):
    strings_to_print = []
    for item in to_print:
        if item == 'energy':
            if bosons_only == True:
                energy = mixture.boson_energy(x, psi) / L
            else:
                energy = mixture.total_energy(x, psi) / L
            strings_to_print.append(f"{energy}")
        elif item == 'peierls_gap' and finite == False:
            peierls_gap = mixture.peierls_gap(x, psi, mult=1)
            strings_to_print.append(f"{peierls_gap}")
        else:
            pass

    print(",".join(strings_to_print), end="", file=file)


def set_sensible_ylim(ax=None, x=None, items_to_plot=[], ylim='best',
                      ylim_margin=0.0, **calculation):

    if ax is None or x is None:
        return

    # get the current xlim and find the corresponding limits
    u, v = ax.get_xlim()
    u = np.argmax(x > u)
    v = np.argmax(x > v)
    ylim_min = min([min(item) for item in items_to_plot])
    ylim_max = max([max(item) for item in items_to_plot])

    ylim_min *= 1 - ylim_margin
    ylim_max *= 1 + ylim_margin

    if ylim == 'best':
        ax.set_ylim(ylim_min, ylim_max)
    if ylim == 'best_lower_0':
        ax.set_ylim(0, ylim_max)
    if isinstance(ylim, list) and len(ylim) == 2:
        ax.set_ylim(*ylim)

 
def plot_densities(ax=None, nq=101, bosons_only=False, filename="densities.pdf",
                   xlabel=True, ylabel=True, n_repeat=1, finite=True,
                   **calculation):

    calculation = set_params_and_data_path(**calculation)
    # print(f"\npath: {calculation['path']}, data_path: {calculation['data_path']}")
    calculation = load_calculation_params(**calculation)
    if ax is None:
        fig, ax_ = plt.subplots()
    else:
        ax_ = ax

    # TODO load from path or data_path ?
    psi = np.loadtxt(os.path.join(calculation['data_path'], "psi.csv"))

    a = 2 * np.pi

    x = positions(**calculation)

    if finite == True:
        mixture = HybridFinite(**calculation['mixture_params'])
    else:
        mixture = HybridInfinite(**calculation['mixture_params'])

    nb = mixture.boson_density(x, psi)
    nf = mixture.fermion_density(x, psi, nq=nq)

    # repeat the densities - useful in the infinite case
    x = positions(**{**calculation, **{'L': calculation['L'] * n_repeat}})

    nb = np.tile(nb, n_repeat)
    nf = np.tile(nf, n_repeat)

    dx = abs(x[1] - x[0])
    Nb = dx * np.sum(nb)
    Nf = dx * np.sum(nf)

    plot2d(ax_, x / a, nb, # / Nb,
           color=boson_density_color,
           label=r"$n_b(x)$",
           **calculation,
    )

    if bosons_only == False:
        ax_b = ax_.twinx()
        plot2d(ax_b, x / a, nf, # / Nf,
               color=fermion_density_color,   # '#00880088',
               label=r"$n_f(x)$",
               **calculation,
        )

        set_sensible_ylim(ax=ax_b, x=x, items_to_plot=[nf],
                          **calculation)

    # else:
    set_sensible_ylim(ax=ax_, x=x, items_to_plot=[nb],
                      **calculation)

    # set axis labels
    if xlabel is True:
        ax_.set_xlabel(r"$x/a$")
    if ylabel == True:
        ax_.set_ylabel(r"$n_b(x)$", color=boson_density_color)
        if bosons_only == False:
            ax_b.set_ylabel(r"$n_f(x)$", color=fermion_density_color)
    elif ylabel == 'bosons':
        ax_.set_ylabel(r"$n_b(x)$", color=boson_density_color)
    elif ylabel == 'fermions':
        ax_b.set_ylabel(r"$n_f(x)$", color=fermion_density_color)

    ax_.spines['left'].set_color(boson_density_color)
    ax_b.spines['right'].set_color(fermion_density_color)
    ax_.tick_params(axis='y', colors=boson_density_color)
    ax_b.tick_params(axis='y', colors=fermion_density_color)

    if ax is None:
        fig.savefig(os.path.join(calculation['path'], filename))
        plt.close(fig)

    return ax_b


def plot_fermion_density_per_site(ax=None, nq=100, bosons_only=False,
                                  finite=True, xlabel=True, ylabel=True,
                                  filename="fermion_density_per_site.pdf",
                                  **calculation):

    calculation = set_params_and_data_path(**calculation)
    calculation = load_calculation_params(**calculation)
    if ax is None:
        fig, ax_ = plt.subplots()
    else:
        ax_ = ax

    psi = np.loadtxt(os.path.join(calculation['data_path'], "psi.csv"))
    x = positions(**calculation)
    mixture = HybridFinite(**calculation['mixture_params'])
    nf = mixture.fermion_density(x, psi, nq=nq)
    nf_per_site = []
    L = calculation['L']
    ns = calculation['ns']
    for i in range(L):
        nif = mixture.integrate(x[i*ns:(i+1)*ns], nf[i*ns:(i+1)*ns])
        nf_per_site.append(nif)
    nf_per_site = np.array(nf_per_site)

    # TODO set linestyle / marker?

    plot2d(ax_, range(-L//2, L//2), nf_per_site, color=fermion_density_color, **calculation)

    set_sensible_ylim(ax=ax_, x=range(-L//2, L//2),
                      items_to_plot=[nf_per_site], **calculation)

    if xlabel == True:
        ax_.set_xlabel(r"$x/a$")
        # ax_.set_xlabel(r"Site $j$")

    if ylabel == True:
        ax_.set_ylabel(r"$n_f(x_j)$", color=fermion_density_color)

    ax_.yaxis.set_label_position("right")
    ax_.yaxis.tick_right()

    if ax is None:
        fig.savefig(os.path.join(calculation['path'], filename))
        plt.close(fig)


def plot_cumulative_fermion_density(ax=None, nq=100, bosons_only=False,
                                    finite=True,
                                    filename="cumulative_fermion_density.pdf",
                                    **calculation):

    calculation = set_params_and_data_path(**calculation)
    calculation = load_calculation_params(**calculation)
    if ax is None:
        fig, ax_ = plt.subplots()
    else:
        ax_ = ax

    psi = np.loadtxt(os.path.join(calculation['data_path'], "psi.csv"))
    x = positions(**calculation)
    mixture = HybridFinite(**calculation['mixture_params'])
    nf = mixture.fermion_density(x, psi, nq=nq)
    nf_cumulative = np.array([
        mixture.integrate(x[:i], nf[:i]) for i in range(2, len(x)-1)
    ])

    ax_.set_xlabel(r"$x/a$")

    plot2d(ax_, x[:-3] / (2 * np.pi), nf_cumulative, color='k', **calculation)

    if ax is None:
        fig.savefig(os.path.join(calculation['path'], filename))
        plt.close(fig)


def plot_fourier_boson(ax=None, n_virtual=1, filename="densities_fourier.pdf",
                       xlabel=True, ylabel=True, **calculation):

    calculation = set_params_and_data_path(**calculation)
    calculation = load_calculation_params(**calculation)
    if ax is None:
        fig, ax_ = plt.subplots()
    else:
        ax_ = ax

    psi = np.loadtxt(os.path.join(calculation['data_path'], "psi.csv"))
    x = positions(**calculation)
    k, psi_k = fourier_transform(x, psi, n_virtual=n_virtual)
    nb_k = np.abs(psi_k)

    plot2d(ax_, k, abs(nb_k), color='k', **calculation)

    set_sensible_ylim(ax=ax_, x=k, items_to_plot=[abs(nb_k)],
                      **calculation)

    if xlabel == True:
        ax_.set_xlabel(r"$k/K$")

    if ax is None:
        fig.savefig(os.path.join(calculation['path'], filename))
        plt.close(fig)


def plot_correlator(ax=None, flavor='abs', xlim=None, xlim_treshold=1e-3,
                    title=False, filename="correlator.pdf", cmap='Greys',
                    tick_fontsize=9, delta=0.5,
                    **calculation):

    calculation = set_params_and_data_path(**calculation)
    calculation = load_calculation_params(**calculation)
    if ax is None:
        fig, ax_ = plt.subplots()
    else:
        ax_ = ax

    psi = np.loadtxt(os.path.join(calculation['data_path'], "psi.csv"))
    x = positions(**calculation)
    if 'finite' in calculation and calculation['finite'] != True:
        mixture = HybridInfinite(**calculation['mixture_params'])
    else:
        mixture = HybridFinite(**calculation['mixture_params'])
    c = mixture.correlator(x, psi)

    if flavor == '1d':

        # find index closest to center with a maximum
        j = np.diag(np.abs(c)).argmax()
        # j = len(x) // 2
        xm = x[j]
        print(f"xm = {xm/(2*np.pi)}")
        # fig, ax = plt.subplots()

        # find maximum 
        i = np.abs(c[:, j]).argmax()
        xi = x[i]
        print(f"xi = {xm/(2*np.pi)}")

        ax_.plot((x - xi) / (2 * np.pi), np.abs(c[:, j]))
        ax_.set_xlabel(r"$(x-x\prime)/a$")
        ax_.set_ylabel(r"$n(x, x\prime)$")

        # TODO fit not working

        # filter out closest elements 
        mask = abs(x - xi)/(2 * np.pi) <= delta
        # fit exponential
        fun = lambda x, a, b: abs(a) * np.exp(-abs(b)*np.abs(x-xi))
        popt, pcov = opt.curve_fit(
            fun,
            x[mask],
            np.abs(c[mask, j]),
        )
        print(popt)
        print(pcov)
        # ax_.plot([xi / (2 * np.pi)], [0], 'ro')

        x_ = np.linspace(min(x), max(x), 100000)
        ax_.plot((x_ - xi) / (2 * np.pi), fun(x_, *popt), ':r')

        # focus on the part which falls off exponentially
        ax_.set_xlim(-delta, delta)

    if flavor == 'diag':

        # find index closest to center with a maximum
        j = np.diag(np.abs(c)).argmax()
        # j = len(x) // 2
        xj = x[j]
        print(f"xj = {xj/(2*np.pi)}")

        # find diagonal values
        xi = 0

        ax_.plot(x / (2 * np.pi), np.abs(np.diag(c[:, ::-1])))
        ax_.set_xlabel(r"$x/a$")
        ax_.set_ylabel(r"$n(x, -x)$")

        # TODO fit not working

        # filter out closest elements 
        mask = abs(x - xi)/(2 * np.pi) <= delta
        # fit exponential
        fun = lambda x, a, b: abs(a) * np.exp(-abs(b)*np.abs(x-xi))
        popt, pcov = opt.curve_fit(
            fun,
            x[mask],
            np.abs(c[mask, j]),
        )
        print(popt)
        print(pcov)
        # ax_.plot([xi / (2 * np.pi)], [0], 'ro')

        x_ = np.linspace(min(x), max(x), 100000)
        ax_.plot((x_ - xi) / (2 * np.pi), fun(x_, *popt), ':r')

        # focus on the part which falls off exponentially
        ax_.set_xlim(-delta, delta)

    else:

        im = abs(c)
        if flavor == 'complex':
            im = complex_hls(c)

        extent = [v / (2 * np.pi) for v in (min(x), max(x), min(x), max(x))]
        ax_.imshow(im, extent=extent, origin='lower', cmap=cmap, vmin=0, vmax=np.max(im))

        ax_.set_xlabel(r"$x/a$")
        ax_.set_ylabel(r"$x\prime/a$")

    if title == True:
        ax_.set_title(r"$|\langle \psi^\dagger(x) \psi(x\prime) \rangle|$")

    try:
        ax_.set_xlim(*xlim)
        ax_.set_ylim(*xlim)
    except TypeError as e:
        print(f"xlim: {xlim} is not an iterable, aborting.")
        print(e)

    ax_.tick_params(axis='x', labelsize=tick_fontsize)
    ax_.tick_params(axis='y', labelsize=tick_fontsize)

    if ax is None:
        fig.savefig(os.path.join(calculation['path'], filename))
        plt.close(fig)


def plot_tof_fermion(ax=None, filename="tof_fermion.pdf", n_virtual=1,
                     **calculation):

    calculation = set_params_and_data_path(**calculation)
    calculation = load_calculation_params(**calculation)
    if ax is None:
        fig, ax_ = plt.subplots()
    else:
        ax_ = ax

    psi = np.loadtxt(os.path.join(calculation['data_path'], "psi.csv"))
    x = positions(**calculation)
    mixture = HybridFinite(**calculation['mixture_params'])
    k_tof, intensity_tof = mixture.tof_fermion(x, psi, n_virtual=n_virtual)

    ax_.set_xlabel(r"$m x / \hbar t$")

    plot2d(ax_, k_tof, intensity_tof, color='k', **calculation)

    if ax is None:
        fig.savefig(os.path.join(calculation['path'], filename))
        plt.close(fig)


def table_plot(path='', nrows=1, ncols=1, cells={}, col_headers=None,
               top=None, bottom=None, wspace=None, hspace=None,
               sharex=False, sharey=False, tight_layout=True,
               filename="table_plot.pdf", **calculation):

    fig, ax = plt.subplots(
        nrows=nrows, ncols=ncols,
        sharex=sharex, sharey=sharey,
    )

    # make sure it is two-indexed
    if nrows == 1 and ncols > 1:
        ax = ax[None, :]        
    elif nrows > 1 and ncols == 1:
        ax = ax[:, None]
    elif nrows == 1 and ncols == 1:
        raise ValueError(f"Must have more than one plot. Now, nrows=")

    for i in range(ncols):
        if col_headers is not None:
            ax[0, i].set_title(col_headers[i])
        for j in range(nrows):
            if f"{i}{j}" in cells:
                print(f"{i}{j}")
                plot = cells[f"{i}{j}"]
                # TODO ugly solution
                ax_b = dispatch_plot(plot, ax=ax[j, i], path=path, **calculation)
                if 'cell_title' in plot:
                    ax[j, i].set_title(plot['cell_title'], loc='left')
                if 'inset' in plot:
                    if (ax_b is not None and
                        'twin_axis' in plot['inset'] and
                        plot['inset']['twin_axis'] == True
                        ):
                        ax_ = ax_b
                    else:
                        ax_ = ax[j, i]
                    ax_inset = inset_axis(ax=ax_, **plot['inset'])
                    dispatch_plot(plot['inset'], ax=ax_inset, path=path,
                                  **calculation)

    if tight_layout == True:
        fig.tight_layout()

    for param_name, param in zip(
        ['top', 'bottom', 'hspace', 'wspace'],
        [top, bottom, hspace, wspace]
        ):
        if param is not None:
            fig.subplots_adjust(**{param_name: param})

    fig.savefig(os.path.join(path, filename))

















# class BornOppenheimer(object):
#
#     def __init__(self, v0=10, r=0.04, gb=0.1, gbf=0.01, omega=0, wall=0,
#                  nf=0.5, nb=10, n=2, epsrel=1e-1, lanczos=False, finite=False,
#                  **kwargs):
#         print("Using old code!")
#         self.r = r
#         self.v0 = v0
#         self.gb = gb
#         self.gbf = gbf
#         self.omega = omega
#         self.wall = wall
#         self.Nf = nf
#         self.Nb = nb
#         self.n = n
#         self.epsrel = epsrel
#         self.lanczos = lanczos
#         self.finite = finite
#
#     def integrate(self, x, f):
#         dx = abs(x[1] - x[0])
#         return dx * np.sum(f)
#
#
#     def differentiate(self, x, f):
#         dx = abs(x[1] - x[0])
#         # TODO take diff into account?
#         return (np.roll(f, 1) - f) / dx
#
#
#     def potential(self, x):
#         v = self.v0 * np.cos(x / 2)**2
#         mb = 1 / 2
#         v += (1/2) * mb * self.omega**2 * x**2
#         v[0] += self.wall
#         v[-1] += self.wall
#         return v
#
#
#     def gpe_potential(self, t, x, psi, nq=101):
#         nf = self.fermion_density(x, psi, nq=nq)
#         return self.gpe_potential_bosons(t, x, psi) + self.gbf * nf
#
#
#     def gpe_potential_bosons(self, t, x, psi):
#         v = self.potential(x)
#         nb = self.boson_density(x, psi)
#         return v + self.gb * nb
#
#
#     def total_energy(self, x, psi):
#         """Calculates the ground state energy from fermions and bosons:
#         $E[\psi] = E_A[\psi] + E_B[\psi]$.
#         """
#         e = self.boson_energy(x, psi) + self.fermion_energy(x, psi)
#         return e
#
#
#     def boson_energy(self, x, psi):
#         """The energy contribution from the bosons, $E_A[\psi]$."""
#         return self.boson_kinetic(x, psi) + self.boson_potential(x, psi)
#
#
#     def boson_kinetic(self, x, psi):
#         dpsi = self.differentiate(x, psi)
#         ek = self.integrate(x, self.Nb * np.abs(dpsi)**2)
#         return np.real(ek)
#
#
#     def boson_potential(self, x, psi):
#         v = self.potential(x)
#         ev = self.integrate(x, v * self.Nb * np.abs(psi)**2)
#         eu = self.integrate(x, (1/2) * self.gb * self.Nb**2 * np.abs(psi)**4)
#         return np.real(ev + eu)
#
#
#     def fermion_potential(self, x, nb):
#         mf = self.r / 2
#         v = (1/2) * mf * self.omega**2 * x**2
#         v[0] += self.wall
#         v[-1] += self.wall
#         v += self.gbf * nb
#         return v
#
#
#     def fermion_hamiltonian(self, k, x, psi):
#         """The Fourier transformed sublattice Hamiltonian $\mathbb{H}_k[psi]$.
#         """
#         dx = abs(x[1] - x[0])
#         M = len(x)
#
#         nb = self.boson_density(x, psi)
#         v = self.fermion_potential(x, nb)
#
#         t = 1 / (self.r * dx**2)
#         h = np.diag(v + 2 * t, k=0) + \
#             np.diag(-t * np.ones(M - 1), k=1) + \
#             np.diag(-t * np.ones(M - 1), k=-1) + 0j
#         # the above part concerns the interior of the sublattice cells, there
#         # is also a hopping term between the cells. Here the sublattice quasi-
#         # momentum k comes in
#         # TODO is this time-consuming?
#         if self.finite == False:
#             h[0, -1] = -t * np.exp(1j * self.n * k)
#             h[-1, 0] = -t * np.exp(-1j * self.n * k)
#         return h
#
#
#     def fermion_energy(self, x, psi):
#         """The energy contribution from the fermions in the effective potential
#         of the bosonic mean-field, $E_A[\psi]$.
#         """
#         if self.finite == True:
#             return self.fermion_energy_finite(x, psi)
#         else:
#             return self.fermion_energy_infinite(x, psi)
#
#
#     def fermion_energy_finite(self, x, psi):
#         es, _ = np.linalg.eigh(self.fermion_hamiltonian(0, x, psi))
#         return np.sum(es[:int(self.Nf)])
#
#
#     def fermion_energy_infinite(self, x, psi):
#         nf = self.Nf / abs(x[0] - x[-1])
#
#         # the filling of the sublattice bands is $m \nf$
#         filling = self.n * nf
#
#         # All calculations are then done with the sublattice spectrum. If
#         # $\nf, m$ are chosen such that \nf = n + 1/m., for n
#         # integer, kf = 0 and we will always fill n sublattice bands.
#         # m sublattice bands correspond to 1 physical band, so there is no
#         # inconsistency - for m=2 and $\nf = n + 1/2$, the gap opens in
#         # the middle of the (n+1):th physical band.
#
#         # the Fermi wavevector folded into the first sublattice Brillouin zone
#         kf = np.pi * (filling % 1)
#         # number of completely filled sublattice bands
#         n_bands = np.int(filling // 1)
#
#         # this is just an effective way of summing up all eigenvalues below the
#         # Fermi wavevector:
#
#         # function to sum n lowest sublattice bands
#         def sum_n_lowest(k, n):
#             return np.sum(self.fermion_bands(k, x, psi, n))
#
#         # in interval [0, kf] sum all sublattice bands
#         e = quad(sum_n_lowest, 0, kf, args=(n_bands + 1,),
#                  epsrel=self.epsrel)[0]
#         # in interval [kf, np.pi] sum every completely filled sublattice band
#         if n_bands >= 1:
#             e += quad(sum_n_lowest, kf, np.pi / self.n, args=(n_bands,),
#                       epsrel=self.epsrel)[0]
#         return 2 * e
#
#     def fermion_energy_infinite_bak(self, x, psi):
#         nf = self.Nf / abs(x[0] - x[-1])
#
#         # the filling of the sublattice bands is $m \nf$
#         filling = self.n * nf
#
#         # All calculations are then done with the sublattice spectrum. If
#         # $\nf, m$ are chosen such that \nf = n + 1/m., for n
#         # integer, kf = 0 and we will always fill n sublattice bands.
#         # m sublattice bands correspond to 1 physical band, so there is no
#         # inconsistency - for m=2 and $\nf = n + 1/2$, the gap opens in
#         # the middle of the (n+1):th physical band.
#
#         # the Fermi wavevector folded into the first sublattice Brillouin zone
#         kf = np.pi * (filling % 1)
#         # number of completely filled sublattice bands
#         n_bands = np.int(filling // 1)
#
#         # this is just an effective way of summing up all eigenvalues below the
#         # Fermi wavevector:
#
#         # function to sum n lowest sublattice bands
#         def sum_n_lowest(k, n):
#             return np.sum(self.fermion_bands(k, x, psi, n))
#
#         # in interval [0, kf] sum all sublattice bands
#         e = quad(sum_n_lowest, 0, kf, args=(n_bands + 1,),
#                  epsrel=self.epsrel)[0]
#         # in interval [kf, np.pi] sum every completely filled sublattice band
#         if n_bands >= 1:
#             e += quad(sum_n_lowest, kf, np.pi / self.n, args=(n_bands,),
#                       epsrel=self.epsrel)[0]
#         return 2 * e
#
#
#     def fermion_bands(self, k, x, psi, n):
#         """The bands above quasi-momentum k of the Fourier transformed
#         sublattice Hamiltonian $\mathbb{H}_k[\psi]$.
#
#         TODO here use scipy.sparse.linalg.eigsh if n=1 ???
#
#         """
#         if self.lanczos == True:
#             return eigsh(self.fermion_hamiltonian(k, x, psi), k=n, which='SA',
#                          return_eigenvectors=False)
#         else:
#             return np.linalg.eigvalsh(self.fermion_hamiltonian(k, x, psi))[0:n]
#
#
#     def fermion_diagonalization(self, k, x, psi):
#         """The ordered eigenvalues and eigenvectors of the Fourier transformed
#         sublattice Hamiltonian $\mathbb{H}_k[\psi]$.
#         """
#         return np.linalg.eigh(self.fermion_hamiltonian(k, x, psi))
#
#
#     def fermion_density_and_spectrum(self, x, psi, nq=101):
#         """Calculates the fermion density
#
#         $n_A(x) = \sum_{n < n_max} |\psi_n(x)|^2$
#
#         where |\psi_n(x)| are eigenfunctions of the single-particle Hamiltonian
#         of the fermions: h[\psi] := (-d^2/dx^2 + Veff[\psi](x)).
#         """
#         # the number of sublattice bands to sum over
#         nf = self.Nf / abs(x[0] - x[-1])
#         n_bands = np.int(self.n * nf)
#         q_values = np.linspace(-np.pi / self.n, np.pi / self.n, nq)
#         dq = q_values[1] - q_values[0]
#         spectrum = np.zeros((len(q_values), len(psi)))
#         nf = np.zeros(len(psi))
#
#         for (i, q) in enumerate(q_values):
#             e, v = self.fermion_diagonalization(q, x, psi)
#             spectrum[i, :] = e
#             nf += dq * np.sum(np.abs(v[:, 0:n_bands])**2, axis=-1)
#
#         return q_values, nf, spectrum
#
#
#     def boson_density(self, x, psi):
#         return self.Nb * np.abs(psi)**2
#
#
#     def fermion_density(self, x, psi, nq=101):
#         if self.finite == True:
#             dx = abs(x[1] - x[0])
#             N = len(x)
#             es, vs = np.linalg.eigh(self.fermion_hamiltonian(0.0, x, psi))
#             n = np.zeros(N)
#             for i in range(int(self.Nf)):
#                 n += np.abs(vs[:, i])**2 / dx
#             return n
#         else:
#             _, nf, _ = self.fermion_density_and_spectrum(x, psi, nq=nq)
#             return nf
#
#
#     def peierls_gap(self, x, psi, mult=1):
#         """Calculates the Peierls gap.
#         """
#         # the number of sublattice bands to calculate
#         # nf = self.Nf / abs(x[0] - x[-1])
#         nf = self.Nf * (2 * np.pi)
#         n_bands = np.int(self.n * nf)+1
#         # the correct sublattice quasimomentum is always at the sublattice BZ
#         # edge
#         q = np.pi / (self.n / mult)
#         b = self.fermion_bands(q, x, psi, n_bands)
#         return b[-1] - b[-2]
