import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import yaml
import numpy as np
import matplotlib.pyplot as plt

from codes.helpers import *
from codes.sweep import *
from codes.multi import *


plt.style.use('style.mplstyle')


def sweep_2d(g_label='gb', location=['mixture_params'], g_latex=None,
            color=None, **calculation):

    g_values = get_sweep_values(
        values = calculation['values'],
        params = calculation['each']['each']['mixture_params'],
        x = g_label,
    )

    calculation['calculations'] = []
    for (i, g) in enumerate(g_values):
        c = dict(
            label = f"{g_label}={g}",
            color = color,
            id = f"{g:0.12f}",
        )
        update_nested_by_path(c, ['each'] + location + [g_label], g)
        calculation['calculations'].append(c)

    # run multi with the prepared calculations
    multi(id_label=g_label, **calculation)


def twoside(**calculation):

    path = calculation['path']

    for (sign, sign_label) in zip([-1, +1], ['negative', 'positive']):

        # set path
        sign_path = os.path.join(path, sign_label)
        calculation['path'] = sign_path
        prepare_path(sign_path)

        # set the reflected variable range
        if 'values' in calculation['each']:
            values = calculation['each']['values']
            if isinstance(values, list):
                # TODO not perfect solution if user specifies negative range
                calculation['each']['values'] = [-v for v in values]
            elif 'linspace' in values:
                # TODO not perfect solution if user specifies negative range
                values['linspace']['start'] *= -1
                values['linspace']['stop'] *= -1
            elif 'follow_das' in values:
                values['follow_das']['sign'] = sign
            elif 'follow_das_v0' in values:
                values['follow_das_v0']['sign'] = sign                
            else:
                raise ValueError("Sweep values not properly specified")
        else:
            raise ValueError("Sweep values not properly specified")

        # set additional variables for this sign, if provided
        if sign_label in calculation:
            update_nested(calculation['each'], calculation[sign_label])

        sweep_2d(**calculation)
