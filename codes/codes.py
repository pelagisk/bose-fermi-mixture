import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from codes.helpers import *
from codes.single import *
from codes.sweep import *
from codes.kosterlitz_thouless import *
from codes.multi import *
from codes.sweep_2d import *
from codes.first_order import *
from codes.gap import *
from codes.fig2 import *
# from codes.fig3 import *
# from codes.fig4 import *
from codes.nfea import (
    plot_nfea_dispersion,
    plot_nfea_potential_landscape,
    sweep_nfea_gaps,
    nfea
    )
from codes.thesis import gap_thesis, phase_diagram_thesis


# TODO move this to run.py?
