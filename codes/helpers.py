import os
import sys
import yaml
import numpy as np
from colorsys import hls_to_rgb


# overlap_label = r"| \langle \psi_b | \psi_f \rangle |^2"
overlap_label = r"\mathcal{O}_{bf}"
peierls_gap_label = r"\Delta_P"

boson_density_color = 'g'
fermion_density_color = 'm'

def prepare_path(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)
    else:
        for root, dirs, files in os.walk(dir):
            for file in files:
                os.remove(os.path.join(root, file))
    # # TODO this does not work recursively!
    # else:
    #     for filename in os.scandir(dir):
    #         os.remove(filename)


class FileAndTerminal(object):

    def __init__(self, file, logging=True):
        self.terminal = sys.stdout
        self.file = file
        self.logging = logging

    def write(self, message):
        if self.logging == True:
            self.terminal.write(message)
        self.file.write(message)

    def flush(self):
        # this flush method is needed for python 3 compatibility.
        # this handles the flush command by doing nothing.
        # you might want to specify some extra behavior here.
        pass

    def close(self):
        self.file.close()


def to_string(number):
    """If number is float, convert it to padded 15 decimal string. For integers
    just write the float representation.
    """
    if isinstance(number, float):
        return f"{number:.15f}"
    else:
        return str(number)


def update_nested_by_path(d, path, v, create=True):
    """Updates a nested dict d where path is a list of all the keys, for example

    update_nested_by_path({'a': {'x': 1, 'b': {'c': 1}}}, ['a', 'b', 'c'], 2)

    updates the nested key 'c' to 2.
    """
    if len(path) == 1:
        d[path[0]] = v
    else:
        k, tail = path[0], path[1:]
        if k in d:
            update_nested_by_path(d[k], tail, v)
        else:
            if create == True:
                d[k] = dict()
                update_nested_by_path(d[k], tail, v)
            else:
                raise KeyError(f"No key '{k}' found in dict {d}")

def update_nested(d, d2):
    """
    """
    for key, value in d2.items():
        if (key in d) and isinstance(value, dict):
            update_nested(d[key], value)
        else:
            d[key] = value


def loop_nested(to_loop, f, *args):
    """Unfolds several iterables into as many for loops. f can only have
    side-effects and no return value.

    E.g., this is equivalent to a double for loop over [1, 2] and [3, 4]:

    loop_nested([[1,2], [3,4]], lambda x, y: print(x, y))
    """
    if len(to_loop) == 0:
        f(args)
    else:
        rest_to_loop, loop = to_loop[:-1], to_loop[-1]
        for arg in loop:
            loop_nested(rest_to_loop, f, arg, *args)


def positions(L=100, ns=8, **calculation):
    # TODO not a symmetric interval - is that ok?
    return np.linspace(-np.pi * L, np.pi * L, ns * L + 1)[:-1]


def fourier_transform(x, y_x, axis=-1, n_virtual=1):
    dx = abs(x[1] - x[0])
    tile = np.ones(len(y_x.shape), dtype='int')
    tile[axis] = n_virtual
    y_k = np.fft.fft(np.tile(y_x, tile), axis=axis)
    f = np.fft.fftfreq(y_k.shape[axis], d=dx)
    y_k = np.fft.fftshift(y_k, axes=axis)
    f = np.fft.fftshift(f)
    k = (2 * np.pi) * f
    return k, y_k


def complex_hls(z):

    r = np.abs(z)
    arg = np.angle(z)

    h = (arg + np.pi)  / (2 * np.pi) + 0.5
    l = 1.0 - 1.0/(1.0 + r**0.3)
    s = 0.8

    c = np.vectorize(hls_to_rgb)(h,l,s) # --> tuple
    c = np.array(c)  # -->  array of (3,n,m) shape, but need (n,m,3)
    c = c.swapaxes(0,2)

    return c


def latex_string(string):
    return (r"$" + string + r"$")


def set_params_and_data_path(data_path=None, params_path=None, **calculation):
    if params_path is None:
        calculation['params_path'] = calculation['path']
    else:
        # works if params_path is a relative path
        calculation['params_path'] = os.path.join(
            calculation['path'], params_path
        )
    if data_path is None:
        calculation['data_path'] = calculation['path']
    else:
        # works if data_path is a relative path
        calculation['data_path'] = os.path.join(
            calculation['path'], data_path
        )

    return calculation


def load_calculation_params(**calculation):
    assert(calculation['params_path'] is not None)
    with open(os.path.join(calculation['params_path'], 'in.yml'), 'r') as f:
        c = yaml.safe_load(f.read())
    calculation = {**calculation, **c}
    if 'mixture_params' in calculation:
        # TODO this can be done in Hybrid and ThomasFermi ?
        calculation['mixture_params'] = set_densities(
            L=calculation['L'],
            **calculation['mixture_params'],
        )
    return calculation


def set_densities(L=100, **params):
    "Properly set the densities depending on if density or filling specified"

    a = 2 * np.pi
    if ('nub' in params) and ('nuf' in params):
        params['nb'] = params['nub'] / a
        params['Nb'] = params['nub'] * L
        params['nf'] = params['nuf'] / a
        params['Nf'] = params['nuf'] * L
    # if linear density specified
    elif ('nb' in params) and ('nf' in params):
        params['nub'] = params['nb'] * a
        params['Nb'] = params['nb'] * L * a
        params['nuf'] = params['nf'] * a
        params['Nf'] = params['nf'] * L * a
    return params
