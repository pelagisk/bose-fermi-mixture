import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import yaml
import numpy as np
import matplotlib.pyplot as plt

from codes.helpers import *
from codes.sweep_2d import *


plt.style.use('style.mplstyle')


def find_transition(x, y, treshold=0.5, direction=1, plot=False, **calculation):
    if direction == 1:
        dy = np.abs(np.diff(y[1:]))
        x_ = x[1:-1]
        xc = x_[np.argmax(dy > treshold)]
    else:
        dy = np.abs(np.diff(y[:-1]))
        x_ = x[1:-1]
        xc = x_[::-1][np.argmax(dy[::-1] > treshold)]
    if plot == True:
        fig, ax = plt.subplots()
        ax.plot(x_, dy, '.-')
        ax.axhline(y=treshold)
        ax.plot([xc], [0], 'or')
        fig.savefig(os.path.join(calculation['path'], 'transition.pdf'))
        plt.close(fig)
    return xc


def first_order(plot=True, **calculation):
    twoside(**calculation)
    if plot == True:
        plot_first_order(**calculation)


def plot_first_order(ax=None, color='m', marker='o', markersize=1, tick_fontsize=9, xlim=None, ylim=None, **calculation):

    calculation = set_params_and_data_path(**calculation)

    if ax is None:
        fig, ax_ = plt.subplots()
    else:
        ax_ = ax

    # load parameters from calculation
    with open(os.path.join(calculation['data_path'], 'in.yml'), 'r') as f:
        c = yaml.safe_load(f.read())
    calculation = {**calculation, **c}
    params = c['each']['each']['mixture_params']


    # extract label of variables
    x_label = calculation['each']['g_label']
    y_label = calculation['g_label']

    x_latex = calculation['each']['g_latex']
    y_latex = calculation['g_latex']

    for (sign, sign_label) in zip([-1, +1], ['negative', 'positive']):

        df = pd.read_csv(
            os.path.join(
                calculation['data_path'],
                sign_label,
                'multi.csv',
                ), header=1, index_col=False
        )

        xc_values = []
        y_values = []
        for label, group in df.groupby(y_label):

            group = group.sort_values(by=x_label)
            x_values = group[x_label].to_numpy()
            overlap_values = group['overlap'].to_numpy()

            # TODO this path should be the same as
            path = os.path.join(
                calculation['data_path'], sign_label, f"{label:0.12f}"
            )
            prepare_path(path)

            xc = find_transition(
                x_values,
                overlap_values,
                direction = sign,
                path = path,
                **calculation['find_transition']
            )
            xc_values.append(xc)
            y_values.append(float(label))


        # plot Das prediction
        y_values_2 = np.linspace(df[y_label].min(), df[y_label].max(), 11)
        x_values_2 = []
        for y in y_values_2:
            params = {
                **params,
                **{y_label: y},
            }
            xc = find_das(x=x_label, **params)
            x_values_2.append(sign * xc)
        # TODO fixme
        ax_.plot(4 * np.array(x_values_2), 4 * np.array(y_values_2), ':k')

        # plot critical lines against g
        # TODO fixme
        ax_.plot(np.array(xc_values) * 4, np.array(y_values) * 4, marker=marker, color=color, markersize=markersize)  # or .-

    ax_.set_xlabel(latex_string(x_latex))
    ax_.set_ylabel(latex_string(y_latex))

    ax_.tick_params(axis='x', labelsize=tick_fontsize)
    ax_.tick_params(axis='y', labelsize=tick_fontsize)

    if xlim is not None:
        # TODO fixme
        a, b = xlim
        ax_.set_xlim(a * 4, b * 4)

    if ylim is not None:
        # TODO fixme
        a, b = ylim
        ax_.set_ylim(a * 4, b * 4)


    if ax is None:
        fig.savefig(os.path.join(calculation['path'], f"first_order.pdf"))
