import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit, minimize
from scipy.sparse.linalg import eigsh
from scipy.integrate import quad


def boson_kinetic_energy(θ, ϕ, nb=10, v0=0, gb=0.1, **params):
    return nb / 8 * (
        3
        + np.cos(2 * θ)
        - 4 * np.cos(θ)**2 * np.cos(2 * ϕ)
    )

def boson_potential_energy(θ, ϕ, nb=10, v0=0, gb=0.1, **params):
    return nb * v0 / 8 * (
        3
        + np.cos(2 * θ)
        - 2 * np.sqrt(2) * np.cos(θ)**2 * np.sin(2 * ϕ)
    )

def boson_interaction_energy(θ, ϕ, nb=10, v0=0, gb=0.1, **params):
    return -gb * nb**2 / 512 * (
        - 498
        + 42 * np.cos(4 * ϕ)
        + 24 * np.cos(2 * ϕ)
        - 96 * np.sqrt(2) * np.sin(2 * ϕ)
        + 90 * np.cos(4 * θ)
        - 24 * np.cos(2 * θ)
        + 28 * np.cos(2 * θ + 4 * ϕ)
        + 4 * np.cos(4 * θ + 2 * ϕ)
        + 7 * np.cos(4 * (θ + ϕ))
        + 16 * np.cos(2 * (θ + ϕ))
        + 4 * np.cos(4 * θ - 2 * ϕ)
        + 16 * np.cos(2 * θ - 2 * ϕ)
        + 7 * np.cos(4 * θ - 4 * ϕ)
        + 28 * np.cos(2 * θ - 4 * ϕ)
        + 48 * np.sqrt(2) * np.sin(4 * θ + 2 * ϕ)
        - 48 * np.sqrt(2) * np.sin(4 * θ - 2 * ϕ)
    )


def boson_energy(θ, ϕ, **params):
    return (
        boson_kinetic_energy(θ, ϕ, **params) +
        boson_potential_energy(θ, ϕ, **params) +
        boson_interaction_energy(θ, ϕ, **params)
    )


def simplified_boson_energy(θ, nb=10, v0=0, gb=0.1, **params):
    return nb/32 * (
        4
        + 12 * v0
        + 8 * np.sin(θ)**2
        + 27 * gb * nb
        - 7 * gb * nb * np.cos(4 * θ)
        - 4 * (gb * nb - v0 + 1) * np.cos(2 * θ)
    )


def fermion_matrix(k, θ, ϕ, r=0.04, gbf=0.0, nb=10, n=10, **params):
    N = 2 * n + 1
    L = (1/r) * (k + np.arange(-n, n + 1))**2 + gbf * nb
    g1 = gbf * nb * θ
    g2 = gbf * nb * ϕ
    a1 = np.diag(np.ones(N - 1), k=1)
    a2 = np.diag(np.ones(N - 2), k=2)
    return np.diag(L) + g1 * (a1 + a1.T) + g2 * (a2 + a2.T)


def fermion_matrix_true(k, θ, ϕ, r=0.04, gbf=0.0, nb=10, n=10, **params):
    N = 2 * n + 1
    g0 = gbf * nb * (
        np.cos(θ)**2
        + np.sin(θ)**2
    )
    g1 = gbf * nb * (
        2 * np.sqrt(2) * np.sin(θ) * np.cos(θ) * np.cos(ϕ)
        + 2 * np.sin(θ) * np.cos(θ) * np.sin(ϕ)
    )
    g2 = gbf * nb * (
        2 * np.sqrt(2) * np.cos(θ)**2 * np.sin(ϕ) * np.cos(ϕ)
        + np.sin(θ)**2
    )
    g3 = gbf * nb * (
        2 * np.sin(θ) * np.cos(θ) * np.sin(ϕ)
    )
    g4 = gbf * nb * (
        np.cos(θ)**2 * np.sin(ϕ)**2
    )
    a1 = np.diag(np.ones(N - 1), k=1)
    a2 = np.diag(np.ones(N - 2), k=2)
    a3 = np.diag(np.ones(N - 3), k=3)
    a4 = np.diag(np.ones(N - 4), k=4)
    L = (1/r) * (k + np.arange(-n, n + 1))**2 + g0
    return (
        np.diag(L)
        + g1/2 * (a1 + a1.T)
        + g2/2 * (a2 + a2.T)
        + g3/2 * (a3 + a3.T)
        + g4/2 * (a4 + a4.T)
    )


def fermion_dispersion(k, θ, ϕ, **params):
    return np.linalg.eigvalsh(fermion_matrix_true(k, θ, ϕ, **params))[0]


def extract_gap(θ, ϕ, **params):
    k = 1/2
    es = np.linalg.eigvalsh(fermion_matrix_true(k, θ, ϕ, **params))
    return es[1] - es[0]


def simplified_fermion_dispersion(k, θ, r=0.04, gbf=0.0, nb=10, **params):
    a = (1/r) * (k - 1)**2 + gbf * nb
    b = (1/r) * k**2 + gbf * nb
    c = gbf * nb * np.sqrt(2) * 2 * np.sin(θ) * np.cos(θ)
    return (a + b - np.sqrt((a - b)**2 + 4*c**2)) / 2


def simplified_fermion_energy(θ, epsrel=1e-2, **params):
    def d(k): return simplified_fermion_dispersion(k, θ, **params)
    return 2 * quad(d, 0, 1/2, epsrel=epsrel)[0]


def fermion_energy(θ, ϕ, epsrel=1e-2, **params):
    def d(k): return fermion_dispersion(k, θ, ϕ, **params)
    return 2 * quad(d, 0, 1/2, epsrel=epsrel)[0]


def total_energy(θ, ϕ, simple=False, **params):
    if simple == True:
        return (
            simplified_boson_energy(θ, **params)
            + simplified_fermion_energy(θ, **params)
        )
    else:
        return (
            boson_energy(θ, ϕ, **params)
            + fermion_energy(θ, ϕ, **params)
        )


def minimize_total_energy(θ, ϕ, **params):
    def e(z): return total_energy(z[0], z[1], **params)
    z0 = (θ, ϕ)
    res = minimize(e, z0, bounds=((0, np.pi/2), (0, np.pi/2)))
    (θ, ϕ) = res.x
    energy = res.fun
    return (energy, θ, ϕ)


def kt_scaling(x, a, b):
    mask = x > 1e-3
    y = np.zeros(len(x))
    y[mask] = a * np.exp(-b/x[mask]**2)
    return y


def fit_gap(x, y, p0=(0.21, 0.17)):
    try:
        popt, pcov = curve_fit(kt_scaling, x, y, p0=p0)
        perr = np.sqrt(np.diag(pcov))
        return popt
    except RuntimeError:
        return p0


def plot_dispersion(θ, ϕ, nk=1001, ax=plt, label=None, **params):
    k_values = np.linspace(-1/2, 1/2, nk)
    e_values = np.zeros(len(k_values))
    for (i, k) in enumerate(k_values):
        e_values[i] = fermion_dispersion(k, θ, ϕ, **params)
    ax.plot(k_values, e_values, label=label)
    ax.set_xlabel(r"$k$")
    ax.set_ylabel(r"$E(k)$")


def plot_nfea_dispersion(**calculation):
    """
    See whether optical lattice has a strong effect on the gap
    Does it matter if we include it?
    Compare plotting the gap with and without ϕ
    """

    params = calculation['mixture_params']
    n = calculation['n']
    θ = calculation['theta']
    if 'phi_values' in calculation:
        ϕ_values = calculation['phi_values']
    else:
        ϕ_values = [calculation['phi']]

    fig, ax = plt.subplots()
    for ϕ in ϕ_values:
        plot_dispersion(θ, ϕ, n=n, ax=ax, label=(r"$\phi = %0.2f$" % ϕ),
                        **params)
    ax.legend()
    plt.title(r"NFEA fermion dispersion for $\theta = %0.2f$" % θ)
    plt.savefig(os.path.join(calculation['path'], "dispersion.pdf"))


# # FERMION ENERGY DEPENDENCE ON PHI
# r = 0.04
# nb = 10
# v = 0.0
# gb = 0.1
# gbf = 0.1
# n = 10
# for θ in [0, 0.1, 0.2, 0.3, 0.4]:
#     ϕ_values = np.linspace(0, 0.2, 101)
#     e_values = [fermion_energy(θ, ϕ, r, gbf, nb, n, epsrel=1e-2) for ϕ in ϕ_values]
#     plt.plot(ϕ_values, e_values, label=(r"$\theta = %0.1f$" % θ))
# plt.legend()
# plt.xlabel(r"$\phi$")
# plt.ylabel(r"$\mathcal{E}_f$")
# plt.title(r"Fermion energy density as a function of $\phi$")
# plt.show()


# # HOW MUCH THE ENERGY CONTRIBUTIONS CHANGES DUE TO THE LATTICE
# r = 0.04
# nb = 10
# v = 0.0
# gb = 0.1
# gbf = 0.1
# n = 10
# gbf = 0.8
# v_values = np.linspace(0, 10, 101)
# boson_energy_values = np.zeros(len(v_values))
# fermion_energy_values = np.zeros(len(v_values))
# (θ, ϕ) = (0.01, 0.01)
# for i, v in enumerate(v_values):
#     (θ, ϕ) = minimize_total_energy(θ, ϕ, nb, v, gb, r, gbf, n, epsrel=1e-2)
#     boson_energy_values[i] = boson_energy(θ, ϕ, nb, v, gb)
#     fermion_energy_values[i] = fermion_energy(θ, ϕ, r, gbf, nb, n, epsrel=1e-2)
# plt.plot(v_values, boson_energy_values / boson_energy_values[0], label=r"Bosons")
# plt.plot(v_values, fermion_energy_values / fermion_energy_values[0], label=r"Fermions")
# plt.legend()
# plt.xlabel(r"$V_0$")
# plt.ylabel(r"$\mathcal{E}(V_0)/\mathcal{E}(0)$")
# plt.title(r"Change in boson and fermion energy due to the lattice")
# plt.show()


def plot_nfea_potential_landscape(**calculation):

    params = calculation['mixture_params']
    n = calculation['n']

    θ_values = np.linspace(0, np.pi / 2, 101)
    ϕ_values = np.linspace(0, np.pi / 2, 101)

    for (label, energy) in zip(
        ["boson", "fermion", "total"],
        [boson_energy, fermion_energy, total_energy]
        ):

        e_values = np.zeros((len(θ_values), len(ϕ_values)))
        minval, θc, ϕc = 1000, 0, 0
        for (i, θ) in enumerate(θ_values):
            for (j, ϕ) in enumerate(ϕ_values):
                e_values[i, j] = energy(θ, ϕ, **params)
                if e_values[i, j] < minval:
                    minval = e_values[i, j]
                    θc, ϕc = θ, ϕ

        extent = [c / np.pi for c in [
            min(ϕ_values),
            max(ϕ_values),
            min(θ_values),
            max(θ_values),
        ]]

        fig, ax = plt.subplots()
        im = ax.imshow(e_values, origin='lower', extent=extent)
        if label == "total":
            ax.plot([ϕc / np.pi] , [θc / np.pi], 'or')
        ax.set_title(f"{label.capitalize()} energy")
        ax.set_xlabel(r"$\phi / \pi$")
        ax.set_ylabel(r"$\theta / \pi$")
        fig.colorbar(im)
        fig.savefig(os.path.join(calculation['path'], f"{label}_energy.pdf"))


def nfea(psi=None, file=sys.stdout, **calculation):
    if psi is None:
        psi = (0.01, 0.001)
    theta, phi = psi
    params = calculation['mixture_params']
    nfea_params = calculation['nfea_params']
    energy, theta, psi = minimize_total_energy(
        theta, phi,
        **{**params, **nfea_params}
    )
    gap = extract_gap(theta, phi, **{**params, **nfea_params})
    print(f"{energy},{theta},{phi},{gap}", end="", file=file)
    return (theta, phi)


def nfea_gaps(theta=0.01, phi=0.001, **params):
    (θ, ϕ) = (theta, phi)
    (θ, ϕ) = minimize_total_energy(θ, ϕ, **params, simple=False)
    return (θ, ϕ)

def sweep_nfea_gaps(plot=True, gbf_cut=0.2, **calculation):

    fig_θ, ax_θ = plt.subplots()
    fig_ϕ, ax_ϕ = plt.subplots()
    ax_θ.set_ylabel(r"$\theta$")
    ax_ϕ.set_ylabel(r"$\phi$")

    params = calculation['mixture_params']
    v0_values = calculation['v0_values']
    start = calculation['gbf']['start']
    stop = calculation['gbf']['stop']
    steps = calculation['gbf']['steps']
    gbf_values = np.linspace(start, stop, steps)
    for v0 in v0_values:

        gbf_values = np.linspace(0.5, 0, 101)
        θ_values = np.zeros(len(gbf_values))
        ϕ_values = np.zeros(len(gbf_values))

        θ, ϕ = 0.01, 0.001
        for i, gbf in enumerate(gbf_values):
            (θ, ϕ) = nfea_gaps(theta=θ, phi=ϕ, v0=v0, gbf=gbf, **params)
            θ_values[i] = θ
            ϕ_values[i] = ϕ

        ax_θ.plot(gbf_values, θ_values, label=r"$V_0 = %0.1f$" % v0)
        ax_ϕ.plot(gbf_values, ϕ_values, label=r"$V_0 = %0.1f$" % v0)

        mask = gbf_values < gbf_cut
        (a, b) = fit_gap(gbf_values[mask], θ_values[mask])
        ax_θ.plot(gbf_values, kt_scaling(gbf_values, a, b), ':', color='grey')
        ax_θ.plot(gbf_values[mask], kt_scaling(gbf_values[mask], a, b), ':k')

    # gbf_values_ = gbf_values[gbf_values > 0.1]
    # Δ_values = (
    #     np.exp(-(12 * gb * nb + 1)/(32 * r * nb * gbf_values_**2))
    #     / (2 * np.sqrt(2) * gbf_values_ * nb * r)
    # )
    # ax_gap.plot(gbf_values_, Δ_values, ':r', label="Analytic")

    for ax in [ax_θ, ax_ϕ]:
        ax.set_xlabel(r"$g_{bf}$")
        ax.legend()

    fig_θ.savefig(os.path.join(calculation['path'], "theta.pdf"))
    fig_ϕ.savefig(os.path.join(calculation['path'], "phi.pdf"))
