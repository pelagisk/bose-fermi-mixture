import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
import matplotlib.pyplot as plt

from codes.helpers import *
from codes.plot import *
from codes.split_operator import imaginary_time_propagate
# from codes.split_operator import imaginary_time_propagate_multi


def save_psi(path, psi):
    for i in range(len(psi)):
        np.savetxt(os.path.join(path, f"psi_{i}.csv"), np.real(psi[i]))


def load_psi(path, n=2):
    psi = []
    for i in range(n):
        psii = np.loadtxt(os.path.join(path, f"psi_{i}.csv"))
        psi.append(psii + 0j)
    return psi


class ThomasFermi(object):
    """
    Describes a finite boson-fermion mixture in the Thomas-Fermi approximation.

    Parameters (default value):

    v0 (= 0): amplitude of the optical lattice (bosons only)
    r (= 0.04): ratio of fermion mass / boson mass
    gb (= 0): boson-boson interaction strength
    gbf (= 0): boson-fermion interaction strength
    omega (= 0): unitless angular frequency of the harmonic confinement (both species)
    wall (= 0): depth of the square well at edges of the system
    Nb (= 100): number of bosons
    Nf (= 10): number of fermions
    """

    def __init__(self, v0=0, r=0.04, gb=0.1, gbf=0.01, omega=0, wall=0,
                 Nb=100, Nf=10, **kwargs):
        self.v0 = v0
        self.r = r
        self.gb = gb
        self.gbf = gbf
        self.omega = omega
        self.wall = wall
        self.Nb = Nb
        self.Nf = Nf

        # self.K = 2 * np.pi
        self.K = 1

        self.gpe_potentials = [
            self.gpe_potential_boson,
            self.gpe_potential_fermion,
        ]

    def diff(self, x, f):
        dx = abs(x[1] - x[0])
        return (np.roll(f, 1) - f) / dx

    def integrate(self, x, f):
        dx = abs(x[1] - x[0])
        return dx * np.sum(f)

    def normalized(self, x, psi):
        for i in range(len(psi)):
            psi[i] /= np.sqrt(self.integrate(x, np.abs(psi[i])**2))
        return psi

    def potential_boson(self, x):
        v = self.v0 * np.sin(self.K*x/2)**2 # TODO cos instead?
        mb = 1 / 2
        v += (1/2) * mb * self.omega**2 * x**2
        v[0] += self.wall
        v[-1] += self.wall
        return v

    def potential_fermion(self, x):
        v = np.zeros(len(x))
        mf = self.r / 2
        v += (1/2) * mf * self.omega**2 * x**2
        v[0] += self.wall
        v[-1] += self.wall
        return v

    def boson_density(self, x, psi):
        psi = self.normalized(x, psi)
        return self.Nb * np.abs(psi[0])**2

    def boson_energy(self, x, psi):
        psi = self.normalized(x, psi)
        nb = self.boson_density(x, psi)
        psib = abs(psi[0])
        dpsi = self.diff(x, psib)
        w = self.Nb * np.abs(dpsi)**2
        ek = self.integrate(x, w)
        vb = self.potential_boson(x)
        mb = 1 / 2
        vb += (1/4) * mb * self.omega**2 * x**2
        eu = self.integrate(x, vb * nb + (1/2) * self.gb * nb**2)
        return ek + eu

    def fermion_density(self, x, psi):
        psi = self.normalized(x, psi)
        return self.Nf * np.abs(psi[1])**2

    def fermion_energy(self, x, psi):
        psi = self.normalized(x, psi)
        nb = self.boson_density(x, psi)
        nf = self.fermion_density(x, psi)
        vf = self.potential_fermion(x)
        mf = self.r / 2
        vf += (1/4) * mf * self.omega**2 * x**2
        ef = vf * nf + np.pi**2/(6*mf) * nf**3 + self.gbf * nb * nf
        return self.integrate(x, ef)

    def total_energy(self, x, psi):
        return self.boson_energy(x, psi) + self.fermion_energy(x, psi)

    def gpe_potential_boson(self, t, x, psi):
        nb = self.boson_density(x, psi)
        nf = self.fermion_density(x, psi)
        vb = self.potential_boson(x)
        return vb + self.gb * nb + self.gbf * nf

    def gpe_potential_fermion(self, t, x, psi):
        nb = self.boson_density(x, psi)
        nf = self.fermion_density(x, psi)
        vf = self.potential_fermion(x)
        mf = self.r / 2
        return vf + np.pi**2/(2*mf)*nf**2 + self.gbf * nb


def initial_state_tf(initial_state='flat', x0_relative=0.5, ns=10, L=100,
                     eta=0.001, r=1, **calculation):
    """A helper function which generates an initial wavefunction
    """
    x = positions(L=L, ns=ns)
    x0 = x0_relative * (max(x) - min(x))

    if initial_state == 'gaussian':
        psi = [np.exp(-(x/x0)**2) + 0j, np.exp(-(x/x0)**2) + 0j]
    elif initial_state == 'gaussian_wiggly':
        psi1 = np.exp(-(x/x0)**2) * (1 + eta * np.sin(x / 2)**(2*r)) + 0j
        psi2 = np.exp(-(x/x0)**2) * (1 + eta * np.sin(x / 2)**(2*r)) + 0j
        psi = [psi1, psi2]
    elif initial_state == 'opposite':
        # useful state to capture phase separation when no trap is present
        psib = np.ones(len(x)) + eta * np.sin(x/L)
        psif = np.ones(len(x)) - eta * np.sin(x/L)
        psi = [psib + 0j, psif + 0j]
    elif initial_state == 'unison':
        # useful state to capture collapse when no trap is present
        psib = np.ones(len(x)) + eta * np.sin(x/L)
        psif = np.ones(len(x)) + eta * np.sin(x/L)
        psi = [psib + 0j, psif + 0j]
    elif initial_state == 'collapsed':
        # useful state to capture collapse when no trap is present
        s = 0.01 * x0
        psib = 1/(1 + np.exp((abs(x) - x0/2)/s))
        psif = 1/(1 + np.exp((abs(x) - x0/2)/s))
        psi = [psib + 0j, psif + 0j]
    elif initial_state == 'comparison':
        x0 = np.sqrt(1000) # / np.pi
        psi = [np.exp(-(x/x0)**2) + 0j, np.exp(-(x/x0)**2) + 0j]
    elif initial_state == 'file':
        psi = load_psi(calculation['path'])
    elif initial_state == 'flat':
        psi = [np.ones(len(x)) + 0j, np.ones(len(x)) + 0j]
    else:
        psi = [np.ones(len(x)) + 0j, np.ones(len(x)) + 0j]

    return psi


def single_tf_itp(kinetic=True, plots=[], psi=None, **calculation):

    itp_params = calculation['itp_params']
    params = calculation['mixture_params']

    # use a Weisäcker kinetic energy term. This smooths convergence and is a
    # valid correction to a hydrodynamic approximation,
    # see http://arxiv.org/abs/cond-mat/0106003v1
    if kinetic == True:
        itp_params['m'] = [1/2, params['r'] / 2]
    else:
        itp_params['m'] = [1/2, 1.0e10]

    if psi is None:
        psi = initial_state_tf(**calculation)

    # TODO minimize first only the bosons??

    # use imaginary time propagation to find the ground state
    mixture = ThomasFermi(**params)
    v = mixture.gpe_potentials
    energy = mixture.total_energy
    x = positions(**calculation)
    psi, es, ds = imaginary_time_propagate(x, psi, v, energy, **itp_params)
    # psi, es, ds = imaginary_time_propagate_multi(x, psi, v, energy, **itp_params)

    # save convergence plots
    np.savetxt(os.path.join(calculation['path'], "convergence_energy.csv"), es)
    np.savetxt(os.path.join(calculation['path'], "convergence_norm.csv"), ds)

    # check if 'convergence' plot included, otherwise add to list of plots
    # in_plots = False
    # for plot in plots:
    #     if plot['type'] == 'convergence':
    #         in_plots = True
    #         break
    # if in_plots == False:
    #     plots.append(dict(
    #         type='convergence',
    #     ))

    return finish_single(x, psi, mixture, plots=plots, **calculation)


def finish_single(x, psi, mixture, plots=[], **calculation):

    # save psi to file
    save_psi(calculation['path'], psi)

    # plot things
    dict_of_plot_functions = dict(
        convergence = plot_convergence,
        densities = plot_densities,
    )
    for plot in plots:
        if plot['type'] in dict_of_plot_functions:
            plot_function = dict_of_plot_functions[plot['type']]
            # note: here, keywords of `plot` overrides those in `calculation`
            plot_function(**{**plot, **calculation})

    # print various measurables to output
    printout(x, psi, mixture, **calculation)

    return psi


def printout(x, psi, mixture, to_print=['energy', 'overlap'], L=2,
             file=sys.stdout, **calculation):
    strings_to_print = []
    for item in to_print:
        if item == 'energy':
            energy = mixture.total_energy(x, psi) / L
            strings_to_print.append(f"{energy}")
        elif item == 'overlap':
            nb = mixture.boson_density(x, psi)
            nf = mixture.fermion_density(x, psi)
            length = 2 * np.pi * L
            c = length / (mixture.Nb * mixture.Nf)
            overlap = c * mixture.integrate(x, nb*nf)
            strings_to_print.append(f"{overlap}")
        elif item == 'participation':
            nb = mixture.boson_density(x, psi)
            length = 2 * np.pi * L
            c = length / (mixture.Nb **2)
            participation = c * mixture.integrate(x, nb**2)
            strings_to_print.append(f"{participation}")
        else:
            pass

    print(",".join(strings_to_print), end="", file=file)


def plot_densities(ax=None, filename="densities.pdf", **calculation):

    psi = load_psi(calculation['path'])
    x = positions(**calculation)
    mixture = ThomasFermi(**calculation['mixture_params'])
    nb = mixture.boson_density(x, psi)
    nf = mixture.fermion_density(x, psi)

    dx = abs(x[1] - x[0])
    Nb = dx * np.sum(nb)
    Nf = dx * np.sum(nf)

    if ax is None:
        fig, ax_ = plt.subplots()
    else:
        ax_ = ax

    ax_.set_xlabel(r"$x/a$")

    plot2d(ax_, x / (2*np.pi), nb / Nb,
           color=boson_density_color,
           label=r"$n_b(x)/N_b$",
           **calculation,
    )

    plot2d(ax_, x / (2*np.pi), nf / Nf,
           color=fermion_density_color,
           label=r"$n_f(x)/N_f$",
           **calculation,
    )

    ax_.set_ylim(0, 1.1 * max(max(nb / Nb), max(nf / Nf)))


    fig.savefig(os.path.join(calculation['path'], filename))
    plt.close(fig)
