import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import yaml
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from codes.helpers import *
from codes.sweep_2d import *


plt.style.use('style.mplstyle')


def gap(plot=True, **calculation):

    # verify that inner sweep is NOT using follow_das, since this case can't be
    # plotted properly
    assert('follow_das' not in calculation['values'])

    twoside(**calculation)
    if plot == True:
        plot_gap(**calculation)


def plot_gap(ax=None, x_latex="g_{bf}", y_latex="g_b", cmap='Greys', xlim=None,
             levels=None, **calculation):

    calculation = set_params_and_data_path(**calculation)
    calculation = load_calculation_params(**calculation)
    if ax is None:
        fig, ax_ = plt.subplots()
    else:
        ax_ = ax

    ax_colorbar = inset_axes(ax_,
        width = "30%",
        height = "5%",
        loc = 'upper right',
    )

    # extract label of variables
    x_label = calculation['each']['g_label']
    y_label = calculation['g_label']

    x_latex = calculation['each']['g_latex']
    y_latex = calculation['g_latex']

    gap_data_values = []

    min_x, max_x, min_y, max_y = 1000, 0, 1000, 0

    for (sign, sign_label) in zip([-1, +1], ['negative', 'positive']):

        df = pd.read_csv(
            os.path.join(
                calculation['data_path'],
                sign_label,
                'multi.csv',
                ), header=1, index_col=False
        )

        x_values = df[x_label].unique()
        y_values = df[y_label].unique()

        min_x = min(min_x, min(x_values))
        max_x = max(max_x, max(x_values))
        min_y = min(min_y, min(y_values))
        max_y = max(max_y, max(y_values))
        gap_data = np.zeros((len(y_values), len(x_values)))

        j = 0
        for label, group in df.groupby(y_label):
            group = group.sort_values(by=x_label)
            # x_values = group[x_label].to_numpy()
            gap_values = group['peierls_gap'].to_numpy()
            gap_data[j, :] = gap_values
            j += 1
        gap_data_values.append(gap_data)

    gap_data = np.concatenate(gap_data_values, axis=-1)
    extent = (min_x, max_x, min_y, max_y)

    # ax_.imshow(gap_data, origin='lower', aspect='auto', cmap=cmap,
    #            extent=extent)

    m, n = gap_data.shape
    x = np.linspace(min_x, max_x, n)
    y = np.linspace(min_y, max_y, m)

    # levels=([0.01 * 2**i for i in range(10)] + [10, 20, 30, 40]),
    contour_kwargs = dict(
        cmap=cmap,
        extent=extent,
    )
    if levels is not None:
        contour_kwargs['levels'] = levels
    contour = ax_.contourf(x, y, gap_data, **contour_kwargs)
    plt.colorbar(contour, cax=ax_colorbar, orientation='horizontal',
                 ticks=[0, 10, 20, 30])
    ax_colorbar.xaxis.set_ticks_position('bottom')


    ax_.set_xlabel(latex_string(x_latex))
    ax_.set_ylabel(latex_string(y_latex))

    # if xlim is not None:
    #     ax_.set_xlim(*xlim)

    if ax is None:
        fig.savefig(os.path.join(calculation['path'], f"gap.pdf"))
