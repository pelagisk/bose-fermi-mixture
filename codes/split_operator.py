import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
from scipy.fft import fft, ifft, fftfreq, fftshift, ifftshift
import matplotlib.pyplot as plt


def imaginary_time_propagate(x, psi, v, energy, callback=None, **kwargs):
    es = []
    ds = []
    def cb(i, t, psi, dpsi):
        es.append(energy(x, psi))
        ds.append(dpsi)
        if callback is not None:
            callback(i, t, psi, dpsi)
    psi = splitoperator(x, psi, v, imaginary_time=True, callback=cb, **kwargs)
    return psi, es, ds


def time_propagate(x, psi, v, **kwargs):
    psi = splitoperator(x, psi, v, callback=callback, **kwargs)
    return psi


def splitoperator(x, psi, v, m=[1], hbar=1, dt=0.05, t0=0, n=100,
                        callback=None, imaginary_time=False, tol=1e-5,
                        noise=0,
                        condition=None):
    """
    Using the split operator Fourier method, time evolves the (non-linear)
    Schrödinger equation for a wavefunction of several components ψi:

    i ∂ψi/∂t = -ħ²/2mi Δ + vi(t, x, [ψ]) ψi

    where ψi, mi, vi are the i:th components of the wavefunction, mass and
    potential vectors. The arguments psi and v are interpreted as lists
    [psi0, psi1, ...], [v0(t, x, v), v1(t, x, v), ...] and m can be a float or
    list.

    If provided, expects a callback of the form

    callback(iteration: int, time: float, ψ)

    """

    # handle the 1d case first
    if callable(v):
        psi = [psi]
        v = [v]
        m = [m]

    if n == 0:
        return psi

    dx = abs(x[1] - x[0])
    N = len(x)
    f = fftshift(fftfreq(N, d=dx))
    k = (2*np.pi) * f
    dk = abs(k[1] - k[0])

    assert(len(psi) == len(v))

    if isinstance(m, float):
        m = [m] * len(psi)

    def copy(psi):
        psi_new = []
        for i in range(len(psi)):
            psi_new.append(np.copy(psi[i]))
        return psi_new

    def integrate(f):
        return dx * np.sum(f)

    def normalized(psi):
        for i in range(len(psi)):
            psi[i] /= np.sqrt(integrate(np.abs(psi[i])**2))
        return psi

    def x_step(dt_, t, psi):
        for i in range(len(psi)):
            vt = v[i](t, x, psi)
            u = np.exp(-1j * vt / hbar * dt_)
            psi[i] *= u
            psi[i] /= np.sqrt(dx * np.sum(np.abs(psi[i])**2))
        return psi

    def k_step(dt_, psi):
        for i in range(len(psi)):
            psi_k = fftshift(fft(psi[i]))
            kinetic = (hbar * k)**2 / (2 * m[i])
            u = np.exp(-1j * kinetic / hbar * dt_)
            psi_k *= u
            psi_k /= np.sqrt(dk * np.sum(np.abs(psi_k)**2))
            psi[i] = ifft(ifftshift(psi_k))
        return psi

    if condition is None:
        condition = lambda i, t, psi, dpsi: True

    dt_ = dt
    if imaginary_time == True:
        dt_ *= -1.0j
    t = t0
    psi = normalized(psi)
    old_psi = copy(psi)
    dpsi = 1e20
    i = 0
    keep_running = True
    while keep_running:

        # the conditions to keep on running
        if imaginary_time == True:
            keep_running = (
                (i < n) and
                (dpsi > tol) and
                condition(i, t, psi, dpsi))
        else:
            keep_running = (i < n)

        # psi = x_step(0.5*dt_, t, psi)
        # psi = k_step(dt_, psi)
        # psi = x_step(0.5*dt_, t, psi)
        # psi = normalized(psi)

        psi = x_step(dt_, t, psi)
        psi = normalized(psi)
        psi = k_step(dt_, psi)

        for j in range(len(psi)):
            psi[j] += noise * np.random.rand(len(psi[j]))

        old_psi = normalized(old_psi)

        psi = normalized(psi)

        # since each component normalized to 1, we must divide by length of vec
        overlap = sum(
            abs(integrate(np.conj(psi[j])*old_psi[j])) for j in range(len(psi))
        ) / len(psi)
        dpsi = 1 - overlap

        if callback is not None:
            callback(i, t, psi, dpsi)

        old_psi = np.copy(psi)
        t += dt
        i += 1

    return psi


# def imaginary_time_propagate_multi(x, psi, v, energy, **kwargs):
#     e_values = []
#     dpsi_values = []
#     # cb = lambda i, t, psi: es.append(energy(x, psi))
#     def cb(i, t, psi, dpsi):
#         e_values.append(energy(x, psi))
#         dpsi_values.append(dpsi)
#     psi = splitoperator_multi(x, psi, v, imaginary_time=True, callback=cb, **kwargs)
#     return psi, e_values, dpsi_values
#
# def time_propagate_multi(x, psi, v, **kwargs):
#     psi = splitoperator_multi(x, psi, v, callback=callback, **kwargs)
#     return psi




# def splitoperator(x, psi, v, m=1, hbar=1, dt=0.05, t0=0, n=100, callback=None,
#                   imaginary_time=False, tol=1e-5, condition=None, **kwargs):
#     """
#     Using the split operator Fourier method, time evolves the (non-linear)
#     Schrödinger equation
#
#     i ∂ψ/∂t = -ħ²/2m Δ + v(t, x, ψ) ψ
#
#     where, for example, v(t, x, ψ) = V(x) + 2 g N |ψ|². If provided, expects a
#     callback of the form
#
#     callback(iteration: int, time: float, ψ)
#
#     """
#     dx = abs(x[1] - x[0])
#     N = len(x)
#     f = fftshift(fftfreq(N, d=dx))
#     k = (2*np.pi) * f
#
#     def integrate(f):
#         return dx * np.sum(f)
#
#     def normalized(psi):
#         psi /= np.sqrt(integrate(np.abs(psi)**2))
#         return psi
#
#     def x_step(dt_, t, psi):
#         vt = v(t, x, psi)
#         u = np.exp(-1j * vt / hbar * dt_)
#         psi *= u
#         return psi
#
#     def k_step(dt_, psi):
#         psi_k = fftshift(fft(psi))
#         kinetic = (hbar * k)**2 / (2 * m)
#         u = np.exp(-1j * kinetic / hbar * dt_)
#         psi_k *= u
#         psi = ifft(ifftshift(psi_k))
#         return psi
#
#     if condition is None:
#         condition = lambda i, t, psi, dpsi: True
#
#     dt_ = dt
#     if imaginary_time == True:
#         dt_ *= -1.0j
#     t = t0
#     psi = normalized(psi)
#     old_psi = np.copy(psi)
#     dpsi = 1000
#     i = 0
#     keep_running = True
#     while keep_running:
#
#         # the conditions to keep on running
#         if imaginary_time == True:
#             keep_running = (
#                 (i < n) and
#                 (dpsi > tol) and
#                 condition(i, t, psi, dpsi))
#         else:
#             keep_running = (i < n)
#
#         # TODO check normalization conditions - when are they needed?
#         # psi = x_step(0.5*dt_, t, psi)
#         # psi = normalized(psi)
#         # psi = k_step(dt_, psi)
#         # psi = normalized(psi)
#         # psi = x_step(0.5*dt_, t, psi)
#         # # psi = x_step(dt_, t, psi)
#         # psi = normalized(psi)
#         psi = x_step(dt_, t, psi)
#         psi = normalized(psi)
#         psi = k_step(dt_, psi)
#         psi = normalized(psi)
#
#         dpsi = 1 - abs(integrate(np.conj(psi)*old_psi))**2
#
#         if callback is not None:
#             callback(i, t, psi, dpsi)
#
#         old_psi = np.copy(psi)
#         t += dt
#         i += 1
#
#     return psi
