import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, InsetPosition
from mpl_toolkits.axes_grid1.inset_locator import mark_inset as do_mark_inset

from codes.helpers import *


plt.style.use('style.mplstyle')


def plot_convergence(convergence_path=None, title='Convergence', **calculation):

    es = np.loadtxt(os.path.join(calculation['path'], "convergence_energy.csv"))
    ds = np.loadtxt(os.path.join(calculation['path'], "convergence_norm.csv"))

    fig, ax = plt.subplots(1, 2)
    fig.suptitle(title)
    ax[0].plot(es)
    ax[0].set_xlabel("Iteration")
    ax[0].set_ylabel("Energy")
    ax[1].plot(ds)
    ax[1].set_xlabel("Iteration")
    ax[1].set_ylabel(r"$1 - | \langle \psi | \psi' \rangle |^2$")
    # mixture.plot_densities(x, psi, ax=ax[2], bosons_only=bosons_only,
    # zoom=zoom, ticks=ticks)
    # ax[2].set_xlabel(r"$x/a$")
    fig.tight_layout()
    if convergence_path is None:
        convergence_path = os.path.join(
            calculation['path'], 'convergence.pdf')
    fig.savefig(convergence_path)
    plt.close(fig)


def plot2d(ax, x, y,
           color='k', linestyle='-', label=None, logscale=False,
           xlim=None, xlim_treshold=1e-3, xlim_margin=0.1,
           ticks=True, tick_labels=True,
           yticks=False, ytick_labels=True, use_yticks=True,
           integer_yticks=False,
           tick_fontsize=9,
           grid=False,
           legend=False,
           aspect=None,
           **calculation):

    if logscale == True:
        ax.semilogy(x, y, color=color, linestyle=linestyle, label=label)
    else:
        ax.plot(x, y, color=color, linestyle=linestyle, label=label)

    if xlim == 'best':
        d = abs(x[np.argmax(y > xlim_treshold)])
        d *= (1 + xlim_margin)
        ax.set_xlim(-d, d)
    else:
        try:
            ax.set_xlim(*xlim)
        except TypeError as e:
            # print(f"xlim: {xlim} is neither 'best' or an iterable, aborting.")
            # print(e)
            pass

    if ticks == True:
        a, b = ax.get_xlim()
        ax.set_xticks(range(int(a), int(b) + 1))
    elif isinstance(ticks, list):
        ax.set_xticks(ticks)
    if tick_labels == False:
        ax.set_xticklabels([])
    elif tick_labels == 'even':
        lbls = [str(i) if i % 2 == 0 else '' for i in ax.get_xticks()]
        ax.set_xticklabels(lbls)

    if yticks == True:
        a, b = ax.get_ylim()
        ax.set_yticks(range(int(a), int(b) + 1))
    elif isinstance(yticks, list):
        ax.set_yticks(yticks)
    if ytick_labels == False:
        ax.set_yticklabels([])
    elif use_yticks == False:
        ax.set_yticks([])
        ax.set_yticklabels([])

    ax.tick_params(axis='x', labelsize=tick_fontsize)
    ax.tick_params(axis='y', labelsize=tick_fontsize)

    if grid == True:
        ax.grid(True)

    if legend == True:
        ax.legend()

    if aspect is not None:
        ax.set_aspect(aspect)


def plot_observable(df, ax, x='gbf', y='energy', plot_label=None, legend=False,
                    color=None, marker=".", linestyle="-", **calculation):
    """
    """

    if 'x_latex' not in calculation:
        calculation['x_latex'] = x

    if 'y_latex' not in calculation:
        calculation['y_latex'] = y

    kwargs = dict(
        x = x,
        xlabel = latex_string(calculation['x_latex']),
        y = y,
        ylabel = latex_string(calculation['y_latex']),
        label = plot_label,
        legend = legend,
        marker = marker,
        linestyle = linestyle,
        ax = ax,
    )
    if color is not None:
        kwargs['color'] = color

    # TODO fixme
    df[x] *= 4
    df.plot(**kwargs)


def plot_compare_das(df, ax, label=None, legend=False,
                     compare_das_both_signs=False, values=[], **calculation):
    params = calculation['each']['mixture_params']
    if 'nuf' in params:
        params['nf'] = params['nuf'] / (2 * np.pi)

    if calculation['g_label'] == 'gb':
        gc = params['gbf']**2 / (2 * np.pi**2 * params['nf'] / params['r'])
    elif calculation['g_label'] == 'gbf':
        gc = np.sqrt(params['gb'] * (2 * np.pi**2 * params['nf'] / params['r']))
        # handle negative values

        # TODO can this be simplified?
        if (isinstance(values, list) and values[0] <= 0 and values[-1] <= 0):
            gc *= -1
        elif ('linspace' in values and
                 (values['linspace']['start'] < 0 or
                  values['linspace']['stop'] < 0)):
            gc *= -1
        elif 'follow_das' in values and values['follow_das']['sign'] < 0:
            gc *= -1
        elif 'follow_das_v0' in values and values['follow_das_v0']['sign'] < 0:
            gc *= -1

    else:
        raise ValueError(
            f"Input variable {sweep_data['g_label']} not compatible with plot"
            )
    # TODO fixme
    gc *= 4
    ax.axvline(x=gc, label=None, color='k', linestyle=':')
    if compare_das_both_signs == True:
        ax.axvline(x=-gc, label=None, color='k', linestyle=':')


def inset_axis(ax=None, lbwh=(0.45, 0.10, 0.5, 0.25), mark_inset=False,
               **calculation):
    if ax is None:
        raise ValueError("No axis on which to add inset")
    ax_inset = plt.axes([0, 0, 1, 1])
    ip = InsetPosition(ax, lbwh)
    ax_inset.set_axes_locator(ip)

    if mark_inset == True:
        do_mark_inset(ax, ax_inset, loc1=1, loc2=2, fc='none', ec='gray')

    return ax_inset
