import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))


import uuid
import numpy as np
from scipy.optimize import curve_fit, basinhopping
import pandas as pd
import matplotlib.pyplot as plt


from codes.helpers import *
from codes.sweep import *


plt.style.use('style.mplstyle')


def plot_kt_transition(ax=None, x_latex="g_{bf}", y_latex="g_b", xlim=None, **calculation):

    calculation = set_params_and_data_path(**calculation)
    calculation = load_calculation_params(**calculation)
    if ax is None:
        fig, ax_ = plt.subplots()
    else:
        ax_ = ax

    # extract label of variables
    x_label = calculation['each']['g_label']
    y_label = calculation['g_label']

    x_latex = calculation['each']['g_latex']
    y_latex = calculation['g_latex']

    assert(x_label == 'gbf')

    min_x, max_x, min_y, max_y = 1000, 0, 1000, 0

    for (sign, sign_label) in zip([-1, +1], ['negative', 'positive']):

        df = pd.read_csv(
            os.path.join(
                calculation['data_path'],
                sign_label,
                'multi.csv',
                ), header=1, index_col=False
        )

        x_values = df[x_label].unique()
        y_values = df[y_label].unique()

        min_x = min(min_x, min(x_values))
        max_x = max(max_x, max(x_values))
        min_y = min(min_y, min(y_values))
        max_y = max(max_y, max(y_values))
        # gap_data = np.zeros((len(y_values), len(x_values)))

        gb_values = []
        gc_values = []

        j = 0
        for label, group in df.groupby(y_label):
            group = group.sort_values(by=x_label)
            x_values = group[x_label].to_numpy()
            gap_values = group['peierls_gap'].to_numpy()

            # if sign_label == 'negative':
            #     plot_ = True
            # else:
            #     plot_ = False
            # # make KT fit here and save critical value
            # _, _, gc = make_kt_fit(x_values, gap_values, plot=plot_, **calculation)

            # make KT fit here and save critical value
            _, _, gc = make_kt_fit(x_values, gap_values, **calculation)

            if min(x_values) < 0:
                gc *= -1

            print("gc = ", gc)

            gc_values.append(gc)

            j += 1

        # TODO fixme
        ax_.plot(np.array(gc_values) * 4, y_values * 4, '.-r')

    ax_.set_xlabel(latex_string(x_latex))
    ax_.set_ylabel(latex_string(y_latex))

    # if xlim is not None:
    #     ax_.set_xlim(*xlim)

    if ax is None:
        fig.savefig(os.path.join(calculation['path'], f"kt_transition.pdf"))


def kt_fit(**calculation):
    sweep(**calculation)
    plot_kt_fit(**calculation)


def plot_kt_fit(ax=None, color=None, fit_up_to=0, xlim=None, ylim=None,
                tick_fontsize=9, linestyle_fit=':k', **calculation):

    calculation = set_params_and_data_path(**calculation)

    if ax is None:
        fig, ax_ = plt.subplots()
    else:
        ax_ = ax

    df = pd.read_csv(
        os.path.join(calculation['data_path'], 'sweep.csv'),
        header=1,
        index_col=False
    )
    kwargs = dict(
        x = 'gbf',
        y = 'peierls_gap',
        xlabel = r"$g_{bf}$",
        ylabel = latex_string(peierls_gap_label),
        label = None,
        legend = False,
        # marker = ".",
        linestyle = "-",
        ax = ax_,
    )
    if color is not None:
        kwargs['color'] = color
    df2 = df.copy()
    df2['gbf'] *= 4
    df2.plot(**kwargs)

    gbf_values = df['gbf'].to_numpy()
    peierls_gap_values = df['peierls_gap'].to_numpy()
    params = make_kt_fit(gbf_values, peierls_gap_values, fit_up_to=fit_up_to,
                         linestyle_fit=linestyle_fit, **calculation)

    if min(df['gbf']) >= 0:
        x_ = np.linspace(min(gbf_values), fit_up_to, 1000)
        fitted_gap = f_kt(x_, *params)
    else:
        x_ = np.linspace(-fit_up_to, max(gbf_values), 1000)
        idxs = np.argsort(x_)
        fitted_gap = f_kt(abs(x_), *params)
        fitted_gap = fitted_gap[idxs]

    # fitted_gap = f_kt(x_, *params)

    # if min(df['gbf']) < 0:
    #     x_ = -x_
    #     idxs = np.argsort(x_)
    #     x_ = x_[idxs]
    #     fitted_gap = fitted_gap[idxs]

    # TODO fixme
    ax_.plot(x_ * 4, fitted_gap, linestyle_fit)

    # _, _, gc = params
    # if min(gbf_values) < 0:
    #     gc *= -1
    # ax_.axvline(x=gc, linestyle='-', color='r')


    if xlim is not None:
        # TODO fixme
        a, b = xlim
        ax_.set_xlim(a * 4, b * 4)
    if ylim is not None:
        ax_.set_ylim(*ylim)

    ax_.tick_params(axis='x', labelsize=tick_fontsize)
    ax_.tick_params(axis='y', labelsize=tick_fontsize)

    if ax is None:
        fig.savefig(os.path.join(calculation['path'], "kt_fit.pdf"))

    return params


def make_kt_fit(x, y, guess=(0.4, 2.0, 0.0), niter=10, verbose=False,
             treshold=0.1, keep_guess=False, simple=False,
             fit_up_to=0, max_gap=0, fname=None, plot=False,
             linestyle_fn='.-', linestyle_fit=':k',
             **calculation):

    # if x is in the negative range
    if min(x) < 0:
        x = abs(x)
        idxs = np.argsort(x)
        x = x[idxs]
        y = y[idxs]

    # fit only up to a specified value
    if fit_up_to > 1e-3:
        selection = x < fit_up_to
        x = x[selection]
        y = y[selection]

    # only consider gap (y) values larger than some max_gap value
    if max_gap > 1e-3:
        selection = y < max_gap
        x = x[selection]
        y = y[selection]

    goal = lambda p: np.linalg.norm((f_kt(x, *p) - y))
    res = basinhopping(goal, guess, niter=niter)
    params = res.x

    if verbose == True:
        print(res)

    (a, b, c) = params

    if plot == True:
        fig, ax = plt.subplots()
        ax.plot(x, y, linestyle_fn)
        x_ = np.linspace(min(x), max(x), 1000)
        ax.plot(x_, f_kt(x_, *params), linestyle_fit)
        ax.set_xlim(0, 5)
        # ax.set_ylim(0, 12)
        # ax.axvline(x=das)

        # TODO there is no longer a critical value to be plotted
        # ax.axvline(x=abs(xc))
        ax.set_title(fname)

        if fname == None:
            id = uuid.uuid4().hex[:8]
            fname = os.path.join(calculation['path'], f"{id}.png")
        else:
            fname = os.path.join(calculation['path'], fname)
        fig.savefig(f"{fname}.png")
        plt.close(fig)

    return (abs(a), abs(b), abs(c))



def f_kt(x, a, b, c):
    """
    Kosterlitz-Thouless form
    """
    a, b, c = abs(a), abs(b), abs(c)
    y = np.zeros(len(x))
    z = x - c
    mask = z > 1e-10
    y[mask] = a * np.exp(-b/np.sqrt(z[mask]))
    return y
