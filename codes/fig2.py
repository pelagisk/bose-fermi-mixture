import os, sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import yaml
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from codes.helpers import *
from codes.first_order import *
from codes.gap import *
from codes.kosterlitz_thouless import *


plt.style.use('style.mplstyle')


def fig2(**calculation):

    fig, ax = plt.subplots(2, 2)
    ax = [ax[0, 0], ax[0, 1], ax[1, 0], ax[1, 1]]

    # 2a. phase diagram in (gbf, gb)

    plot_kt_transition(ax = ax[0], **{**calculation, **calculation['fig2a']['kt']})

    for c in calculation['fig2a']['first_order']:
        plot_first_order(ax = ax[0], **{**calculation, **c})

    for a in calculation['fig2a']['annotations']:
        # TODO fixme
        x, y = a['xy']
        a['xy'] = x*4, y*4
        ax[0].annotate(**a)

    # 2b. phase diagram in (gbf, V0)

    plot_kt_transition(ax = ax[1], **{**calculation, **calculation['fig2b']['kt']})

    for c in calculation['fig2b']['first_order']:
        plot_first_order(ax = ax[1], **{**calculation, **c})

    for a in calculation['fig2b']['annotations']:
        # TODO fixme
        x, y = a['xy']
        a['xy'] = x*4, y*4
        ax[1].annotate(**a)

    # 2c. KT fits
    fig2c(ax=ax[2], path=calculation['path'], **calculation['fig2c'])
    
    # 2d. densities
    plot_densities(ax=ax[3], **{**calculation, **calculation['fig2d']})

    # set titles and adjust plot

    for letter, axi in zip('abcd', ax):
        axi.set_title(f"({letter})", loc='left')

    fig.tight_layout()

    fig.savefig(os.path.join(calculation['path'], f"fig2.pdf"))


def fig2c(ax=plt, **calculation):

    calculation['params_path'] = os.path.join(
        calculation['path'],
        calculation['params_path']
    )

    p = os.path.join(calculation['params_path'], 'in.yml')
    with open(p, 'r') as f:
        c = yaml.safe_load(f.read())

    each = c['each']
    each['path'] = calculation['params_path']

    params_values = []

    cm = plt.cm.get_cmap("viridis")

    for i, sub in enumerate(c['calculations']):
        sub['id'] = f"{i:04d}"
        data_path = sub['id']
        each['color'] = sub['color']
        if isinstance(calculation['fit_up_to'], list):
            fit_up_to = calculation['fit_up_to'][i]
        else:
            fit_up_to = calculation['fit_up_to']
        if isinstance(calculation['guess'], list):
            guess = calculation['guess'][i]
        else:
            guess = calculation['guess']

        params = plot_kt_fit(
            ax = ax,
            data_path = data_path,
            fit_up_to = fit_up_to,
            guess = guess,
            xlim = calculation['xlim'],
            ylim = calculation['ylim'],
            linestyle_fit = calculation['linestyle_fit'],
            **each
        )
        params_values.append(params)